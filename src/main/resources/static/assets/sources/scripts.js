var initToolTips = function() {
    $('[data-toggle="tooltip"]').tooltip();
};

var initCheckBoxClick = function() {
    $('.checkbox label').on('click', function() {
        if($(this).find('input').prop('checked') == true) {
            $('.fileoverlay').removeClass('hide');
        } else {
            $('.fileoverlay').addClass('hide');
        }
    });
};

var initProgressTrigger = function() {
    $('.progress-trigger').on('click', function() {
        if($('input[name="no_file_provided"]').prop('checked') == true || $('input[name="file"]').val() != '') {
            showProgressBarAndDisableForm();
        }
    });
};

var showProgressBarAndDisableForm = function() {
    $('.alert-warning').addClass('hide');
    $('.alert-info').removeClass('hide');
    $('.fileoverlay').removeClass('hide');
    $('input[type="submit"]').prop('disabled', true);
};

var hideProgressBarAndEnableForm = function() {
    $('.alert-warning').removeClass('hide');
    $('.alert-info').addClass('hide');
    $('.fileoverlay').addClass('hide');
    $('input[type="submit"]').prop('disabled', false).removeAttr('disabled');
};

$(function () {
    initToolTips();
    initCheckBoxClick();
    initProgressTrigger();
});

$(window).on('unload', function(){
    hideProgressBarAndEnableForm();
});
