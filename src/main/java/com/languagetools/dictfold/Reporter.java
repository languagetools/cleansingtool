package com.languagetools.dictfold;

import java.util.Locale;
import java.util.logging.Logger;

public class Reporter {

    private final static Logger LOGGER = Logger.getLogger(Reporter.class.getName());

    private final String processedMessage;
    private final boolean countErrors;
    private int errors = 0;
    private int processed = 0;
    private long startTime = System.currentTimeMillis();
    private int processInterval;


    public Reporter(String processedMessage, boolean countErrors, int processInterval) {
        this.processedMessage = processedMessage;
        this.countErrors = countErrors;
        this.processInterval = processInterval;
    }

    public Reporter() {
        this("processed", false, 1000);
    }

    public void processed() {
        processed++;
        if (processed % processInterval == 0) {
            LOGGER.info(String.format(Locale.GERMANY, "%s %s terms per second. ",
                    processedMessage.substring(0, 1).toUpperCase() + processedMessage.substring(1),
                    1000 * 1000 / (System.currentTimeMillis() - startTime)));
            LOGGER.info(String.format(Locale.GERMANY, "%s lines %s.", processed, processedMessage));
            logErrorsIfConfigured();
            startTime = System.currentTimeMillis();
        }
    }


    private void logErrorsIfConfigured() {
        if (countErrors) {
            LOGGER.info(String.format(Locale.GERMANY, "%s errors found.", errors));
        }
    }


    public void done() {
        LOGGER.info(String.format(Locale.GERMANY, "DONE! %s lines %s.", processed, processedMessage));
        logErrorsIfConfigured();
    }


    public void error() {
        errors++;
    }
}
