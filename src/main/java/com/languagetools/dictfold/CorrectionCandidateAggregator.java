package com.languagetools.dictfold;


import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import java.util.Collection;
import java.util.List;

public class CorrectionCandidateAggregator {


    public List<CorrectionCandidate> aggregateCorrectionCandidatesByUsedFolder(Collection<CorrectionCandidate> correctionCandidates) {
        List<CorrectionCandidate> aggregatedCandidates = Lists.newArrayList();
        Multimap<List<TermFrequency>, String> candidatesDifferingInUsedFolder = ArrayListMultimap.create();
        for (CorrectionCandidate candidate : correctionCandidates) {
            String folder = candidate.getUsedFolders().size() > 0 ? candidate.getUsedFolders().get(0) : null;
            candidatesDifferingInUsedFolder.put(Lists.newArrayList(candidate.getSourceTerm(), candidate.getTargetTerm()), folder);
        }

        for(List<TermFrequency> key : candidatesDifferingInUsedFolder.keySet()) {
            String[] usedFolders = Iterables.toArray(candidatesDifferingInUsedFolder.get(key), String.class);
            aggregatedCandidates.add(CorrectionCandidate.of(key.get(0), key.get(1), usedFolders));
        }
        return aggregatedCandidates;
    }
}
