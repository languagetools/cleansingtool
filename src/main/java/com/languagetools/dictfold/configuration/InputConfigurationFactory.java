package com.languagetools.dictfold.configuration;


import com.google.common.io.Files;
import org.apache.commons.configuration.Configuration;

import java.io.File;
import java.util.logging.Logger;

public abstract class InputConfigurationFactory extends ConfigurationFactory {

    private final static Logger LOGGER = Logger.getLogger(InputConfigurationFactory.class.getName());

    private static File tempDirectory;
    static {
        tempDirectory = Files.createTempDir(); //TODO: only create if needed?
        tempDirectory.deleteOnExit();
    }
    protected final File indexDirectory;

    public InputConfigurationFactory(Configuration configuration) {
        super(configuration);
        indexDirectory = getIndexDirectoryFromConfiguration();
    }

    //TODO: think about test strategy
    protected File getIndexDirectoryFromConfiguration() {
        String indexDirectoryPropertyValue = configuration.getString("indexer.directory", null);
        File indexDirectory = null;
        if (indexDirectoryPropertyValue == null) {
            indexDirectory = tempDirectory;
            LOGGER.warning("No index directory given, using " + indexDirectory.getAbsolutePath()
                    + ". This directory will be deleted on successful program completion. ");
        } else {
            indexDirectory = new File(indexDirectoryPropertyValue);
        }
        return indexDirectory;
    }

    protected String getSeparatorFromProperties() {
        return configuration.getString("file.separator", "\t"); //TODO: rename? only valid for input!
    }
}
