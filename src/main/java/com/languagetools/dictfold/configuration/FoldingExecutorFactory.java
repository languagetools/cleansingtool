package com.languagetools.dictfold.configuration;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.languagetools.dictfold.*;
import com.languagetools.dictfold.match.AbstractFolder;
import com.languagetools.dictfold.match.exact.ExactMatchFolder;
import com.languagetools.dictfold.match.exact.ExtendedExactMatchFolder;
import com.languagetools.dictfold.match.fuzzy.LevenshteinFolder;
import com.languagetools.dictfold.match.name.LastNameMatchFolder;
import com.languagetools.dictfold.match.fuzzy.NameLevenshteinFolder;
import com.languagetools.dictfold.match.name.NameVariantMatchFolder;
import org.apache.commons.configuration.Configuration;

import java.util.Map;


public class FoldingExecutorFactory extends InputConfigurationFactory {


    private ImmutableMap<String, AbstractFolder> idToDefaultFolder;


    public FoldingExecutorFactory(Configuration configuration) {
        super(configuration);

        idToDefaultFolder = new ImmutableMap.Builder<String, AbstractFolder>()
                .put("levenshtein", new LevenshteinFolder(indexDirectory,
                                    new LevenshteinConfiguration.Builder().build()))
                .put("levenshtein.names", new NameLevenshteinFolder(indexDirectory,
                            new LevenshteinConfiguration.Builder().applyToFullNameCandidates(true).build()))
                .put("exact", new ExactMatchFolder(indexDirectory))
                .put("exact.extended", new ExtendedExactMatchFolder(indexDirectory))
                .put("lastname", new LastNameMatchFolder(indexDirectory))
                .put("namevariants", new NameVariantMatchFolder(indexDirectory)).build();
    }


    public FoldingExecutor createFoldingExecutor() throws Exception {
        Map<String, AbstractFolder> folders = Maps.newHashMap();

        boolean allFoldersEnabled = configuration.getBoolean("folders.enable.all", false);
        if (allFoldersEnabled) {
            folders.putAll(idToDefaultFolder);
        }

        for(Map.Entry<String, AbstractFolder> entry :  idToDefaultFolder.entrySet()) {
            String enableFolderProperty = "folders.enable." + entry.getKey();
            if(configuration.containsKey(enableFolderProperty)) {
                AbstractFolder folderToProcess = entry.getValue();
                boolean folderShallBeContained = configuration.getBoolean(enableFolderProperty);
                if(folderShallBeContained) {
                    folders.put(entry.getKey(), folderToProcess);
                } else {
                    folders.remove(entry.getKey());
                }
            }
        }
        applyLevenshteinProperties(folders);

        FoldingExecutor foldingExecutor = new FoldingExecutor(getSeparatorFromProperties(), folders.values());
        return foldingExecutor;
    }


    public void applyLevenshteinProperties(Map<String, AbstractFolder> folders) {
        LevenshteinFolder levenshteinFolder = (LevenshteinFolder) folders.get("levenshtein");

        if (levenshteinFolder != null){
            folders.put("levenshtein",
                    new LevenshteinFolder(indexDirectory, createLevenshteinConfigurationFromProperties(false)));
        }

        NameLevenshteinFolder nameLevenshteinFolder = (NameLevenshteinFolder) folders.get("levenshtein.names");

        if (nameLevenshteinFolder != null){
            folders.put("levenshtein.names",
                    new NameLevenshteinFolder(indexDirectory, createLevenshteinConfigurationFromProperties(true)));
        }


    }


    public LevenshteinConfiguration createLevenshteinConfigurationFromProperties(boolean applyLevenshteinToFullNameCandidates) {
        LevenshteinConfiguration.Builder levenshteinConfigBuilder = new LevenshteinConfiguration.Builder();

        if (configuration.containsKey("folders.levenshtein.minimalTermLength")) {
            levenshteinConfigBuilder.minimalTermLength(configuration.getInt("folders.levenshtein.minimalTermLength"));
        }

        if (configuration.containsKey("folders.levenshtein.frequencyThreshold")) {
            levenshteinConfigBuilder.frequencyThreshold(configuration.getInt("folders.levenshtein.frequencyThreshold"));
        }

        if (configuration.containsKey("folders.levenshtein.maxEdit")) {
            levenshteinConfigBuilder.maximalEditDistance(configuration.getInt("folders.levenshtein.maxEdit"));
        }

        if (configuration.containsKey("folders.levenshtein.prefixLength")) {
            levenshteinConfigBuilder.prefixLength(configuration.getInt("folders.levenshtein.prefixLength"));
        }

        levenshteinConfigBuilder.applyToFullNameCandidates(applyLevenshteinToFullNameCandidates);
        return levenshteinConfigBuilder.build();
    }
}
