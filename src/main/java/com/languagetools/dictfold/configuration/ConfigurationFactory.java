package com.languagetools.dictfold.configuration;


import com.google.common.base.Preconditions;
import org.apache.commons.configuration.Configuration;

public abstract class ConfigurationFactory {

    protected Configuration configuration;

    public ConfigurationFactory(Configuration configuration) {
        Preconditions.checkNotNull(configuration);
        this.configuration = configuration;
    }
}
