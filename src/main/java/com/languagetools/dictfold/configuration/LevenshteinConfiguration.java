package com.languagetools.dictfold.configuration;


import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class LevenshteinConfiguration {


    private int minimalTermLength;
    private int frequencyThreshold;
    private int maximalEditDistance;
    private int prefixLength;
    private boolean applyToFullNameCandidates;

    private LevenshteinConfiguration(int minimalTermLength, int frequencyThreshold, int maximalEditDistance,
                                     int prefixLength, boolean applyToFullNameCandidates){
        this.minimalTermLength = minimalTermLength;
        this.frequencyThreshold = frequencyThreshold;
        this.maximalEditDistance = maximalEditDistance;
        this.prefixLength = prefixLength;
        this.applyToFullNameCandidates = applyToFullNameCandidates;
    }


    public int getFrequencyThreshold() {
        return frequencyThreshold;
    }

    public int getMinimalTermLength() {
        return minimalTermLength;
    }


    public int getMaximalEditDistance() {
        return maximalEditDistance;
    }


    public int getPrefixLength() {
        return prefixLength;
    }


    public boolean shallApplyToFullNameCandidates() {
        return applyToFullNameCandidates;
    }


    public static class Builder {

        private int minimalTermLength = 7;
        private int frequencyThreshold = 10;
        private int maximalEditDistance = 1;
        private int prefixLength = 1;
        private boolean applyToFullNameCandidates = false;


        public Builder minimalTermLength(int minimalTermLength) {
            this.minimalTermLength = minimalTermLength;
            return this;
        }


        public Builder frequencyThreshold(int frequencyThreshold) {
            this.frequencyThreshold = frequencyThreshold;
            return this;
        }


        public Builder maximalEditDistance(int maximalEditDistance) {
            this.maximalEditDistance = maximalEditDistance;
            return this;
        }


        public Builder prefixLength(int prefixLength) {
            this.prefixLength = prefixLength;
            return this;
        }


        public Builder applyToFullNameCandidates(boolean applyToFullNameCandidates) {
            this.applyToFullNameCandidates = applyToFullNameCandidates;
            return this;
        }


        public LevenshteinConfiguration build() {
            return new LevenshteinConfiguration(minimalTermLength, frequencyThreshold, maximalEditDistance,
                    prefixLength, applyToFullNameCandidates);
        }
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }


    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
