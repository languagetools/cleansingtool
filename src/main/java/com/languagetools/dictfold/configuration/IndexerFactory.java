package com.languagetools.dictfold.configuration;


import com.languagetools.dictfold.Indexer;
import org.apache.commons.configuration.Configuration;

public class IndexerFactory extends InputConfigurationFactory {


    public IndexerFactory(Configuration configuration) {
        super(configuration);
    }


    public Indexer createIndexer() {
        return new Indexer(getSeparatorFromProperties(), indexDirectory);
    }

}
