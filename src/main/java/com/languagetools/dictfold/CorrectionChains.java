package com.languagetools.dictfold;


import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;

import java.io.Writer;
import java.util.Collection;
import java.util.List;
import java.util.SortedSet;


public class CorrectionChains {


    private List<SortedSet<TermFrequency>> correctionChains;


    private CorrectionChains(Collection<CorrectionCandidate> correctionCandidates) {
        correctionChains = createCorrectionChains(correctionCandidates);
    }


    public static CorrectionChains of(Collection<CorrectionCandidate> correctionCandidates) {
        return new CorrectionChains(correctionCandidates);
    }


    public void writeToIndexFormat(Writer writer) {

        try {
            for (SortedSet<TermFrequency> correctionChain : correctionChains) {
                //String chainedCorrectionString = correctionChain.stream().map(e -> replaceCommata(e.getTerm())).collect(Collectors.joining(","));
                //String chainedCorrectionString = correctionChain.stream().map(TermFrequency::getTerm).collect(Collectors.joining(","));
                String chainedCorrectionString = FluentIterable.from(correctionChain).transform(new Function<TermFrequency, String>() {
                    @Override
                    public String apply(TermFrequency input) {
                        return replaceCommata(input.getTerm());
                    }
                }).join(Joiner.on(","));

                writer.write(chainedCorrectionString);
                writer.write("\n");
            }

            writer.flush();
        } catch (Exception exx) {
            throw new IllegalArgumentException("Error occurred while writing correction Chain", exx);
        }
    }


    private List<SortedSet<TermFrequency>> createCorrectionChains(Collection<CorrectionCandidate> correctionCandidates) {
        List<SortedSet<TermFrequency>> correctionChains = Lists.newArrayList();

        for (CorrectionCandidate candidate : correctionCandidates) {
            boolean foundMatchingChain = false;
            if (candidate.getCorrectionStatus() != CorrectionCandidate.Approved.YES) {
                continue;
            }

            for (SortedSet<TermFrequency> correctionChain : correctionChains) {
                if (correctionChain.contains(candidate.getSourceTerm())) {
                    foundMatchingChain = true;
                    correctionChain.add(candidate.getTargetTerm());
                }
                else if (correctionChain.contains(candidate.getTargetTerm())) {
                    foundMatchingChain = true;
                    correctionChain.add(candidate.getSourceTerm());
                }
            }
            if (!foundMatchingChain){
                SortedSet<TermFrequency> chain = Sets.newTreeSet(new TermFrequency.FrequencySetComparator());
                chain.add(candidate.getSourceTerm());
                chain.add(candidate.getTargetTerm());
                correctionChains.add(chain);
            }
        }
        return correctionChains;
    }


    private String replaceCommata(String term) {
        return StringUtils.normalizeSpace(term.replaceAll(",", " "));
    }
}
