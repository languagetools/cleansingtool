package com.languagetools.dictfold;

import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.sort;


public class CorrectionCandidate {  //TODO: think about builder

    public enum Approved {
        YES("J", "Y") , NO("N"), OPEN("");

        private List<String> representation;

        Approved(String... representation) {
            this.representation = Lists.newArrayList(representation);
        }

        public static Approved of(String representation){
            try {
                for (Approved approved : values()) {
                    for (String variant : approved.representation)
                        if (StringUtils.trimToEmpty(representation).equalsIgnoreCase(variant)) {
                            return approved;
                        }
                }
            } catch (Exception exx) {
                //do nothing
            }
            return OPEN;
        }


        @Override
        public String toString() {
            return representation.get(0);
        }
    }

    private TermFrequency sourceTerm;
    private TermFrequency targetTerm;
    private List<String> usedFolders;
    private Approved correctionStatus;
    private String comment;


    protected CorrectionCandidate(TermFrequency sourceTerm, TermFrequency targetTerm, Approved correctionStatus, String comment, String... usedFolder) {
        this.sourceTerm = sourceTerm;
        this.targetTerm = targetTerm;
        this.correctionStatus = correctionStatus;
        this.comment = comment;
        this.usedFolders = Lists.newArrayList(usedFolder);
        sort(this.usedFolders);
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }


    public static CorrectionCandidate of(TermFrequency sourceTerm, TermFrequency targetTerm, String... usedFolder) {
        return of(sourceTerm, targetTerm, Approved.OPEN, "", usedFolder);
    }


    public static CorrectionCandidate of(TermFrequency sourceTerm, TermFrequency targetTerm, Approved correctionStatus,
                                         String comment, String... usedFolder) {
        return new CorrectionCandidate(sourceTerm, targetTerm, correctionStatus, comment, usedFolder);
    }


    public void approve() {
        correctionStatus = Approved.YES;
    }

    public void markAsDuplicate() {
        comment = "-DUPLIKAT-" + StringUtils.trimToEmpty(comment);
    }


    public TermFrequency getTargetTerm() {
        return targetTerm;
    }


    public TermFrequency getSourceTerm() {
        return sourceTerm;
    }


    public List<String> getUsedFolders() {
        return Collections.unmodifiableList(usedFolders);
    }


    public Approved getCorrectionStatus() {
        return correctionStatus;
    }


    public String getComment() {
        return comment;
    }


    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }


    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}

