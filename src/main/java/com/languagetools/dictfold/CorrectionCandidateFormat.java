package com.languagetools.dictfold;


import java.util.List;

public interface CorrectionCandidateFormat {

    List<CorrectionCandidate> readCorrectionCandidates() throws Exception;
    List<CorrectionCandidate> readAutomaticCorrections() throws Exception;
    void writeCandidates(List<CorrectionCandidate> corrections, List<CorrectionCandidate> automaticCorrections);




}


