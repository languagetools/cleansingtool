package com.languagetools.dictfold;


import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class ExcelFormat {


    public interface AbstractColumnName {

        String getName();

        boolean isUnknown();
    }


    private final static Logger LOGGER = Logger.getLogger(ExcelFormat.class.getName());

    protected final File inputFile;
    private final boolean useNewExcelFormat;


    public ExcelFormat(File inputFile) {
        this.inputFile = inputFile;
        useNewExcelFormat = shallNewExcelFormatBeUsed(inputFile);
    }

    public static ExcelFormat.AbstractColumnName getColumnNameFrom(String name, ExcelFormat.AbstractColumnName[] values) {
        for (ExcelFormat.AbstractColumnName column : values){
            if (name.equalsIgnoreCase(column.getName())){
                return column;
            }
        }
        return null;
    }

    public static Set<? extends AbstractColumnName> validColumnNames(AbstractColumnName[] allValues) {
        Set<AbstractColumnName> validColumnNames = Sets.newHashSet();
        for (AbstractColumnName columnName : allValues) {
            if (!columnName.isUnknown()) {
                validColumnNames.add(columnName);
            }
        }
        return validColumnNames;
    }

    protected void logIfCurrentSheetIsEmpty(Sheet sheet, Iterator<Row> rowIterator) {
        if (!rowIterator.hasNext()) {
            LOGGER.log(Level.WARNING, "Found empty sheet in correction input file: " + sheet.getSheetName());
        }
    }

    protected Map<? extends AbstractColumnName, Integer> getColumnNameAndCellIndex(Row firstRow) {
        Map<AbstractColumnName, Integer> columnNameToColumnIndex = Maps.newHashMap();
        Iterator<Cell> cellIterator = firstRow.cellIterator();
        while(cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            try {
                AbstractColumnName columnName = columnNameOf(cell.getStringCellValue());
                if (!columnName.isUnknown()) {
                    columnNameToColumnIndex.put(columnName, cell.getColumnIndex());
                }
            } catch (Exception exx){
                LOGGER.log(Level.WARNING, "Could not read column name", exx);
            }
        }
        Map<? extends AbstractColumnName, Integer> columnNameToIndex = columnNameToColumnIndex;
        Preconditions.checkArgument(columnNameToIndex.keySet().equals(validColumns()), "Not all neccessary columns found. Check the column definitions.");
        return columnNameToIndex;
    }


    protected Integer getColumnAsInt(AbstractColumnName columnName, Map<? extends AbstractColumnName, Integer> columnNameToIndex, Row row) {
        Cell cell = getCellForColumnName(columnName, columnNameToIndex, row);
        return (Integer) (int) cell.getNumericCellValue();
    }


    protected String getColumnAsString(AbstractColumnName columnName, Map<? extends AbstractColumnName, Integer> columnNameToIndex, Row row, boolean columnIsOptional) {
        Cell cell = getCellForColumnName(columnName, columnNameToIndex, row);
        return cell == null && columnIsOptional ? "" : cell.getStringCellValue();
    }


    protected Cell getCellForColumnName(AbstractColumnName columnName, Map<? extends AbstractColumnName, Integer> columnNameToIndex, Row row) {
        return row.getCell(columnNameToIndex.get(columnName));
    }


    protected boolean shallNewExcelFormatBeUsed(File inputFile) {
        if (inputFile != null) {
            String name = inputFile.getName();
            return name.endsWith(".xlsx");
        }
        return false;
    }


    protected Workbook getWorkbook() throws IOException, InvalidFormatException {
        Workbook workbook;
        if (useNewExcelFormat){
            workbook = new XSSFWorkbook(inputFile);
        } else {
            workbook = new HSSFWorkbook(new FileInputStream(inputFile));
        }
        return workbook;
    }


    protected abstract Set<? extends AbstractColumnName> validColumns();


    protected abstract AbstractColumnName columnNameOf(String stringCellValue);

}
