package com.languagetools.dictfold;


import com.google.common.collect.Maps;
import com.languagetools.dictfold.match.name.NameMatchUtils;
import com.languagetools.dictfold.vlb.VlbCorrectionTool;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

public class Indexer {

    private final static Logger LOGGER = Logger.getLogger(VlbCorrectionTool.class.getName());

    public static final String FREQUENCY_FIELD = "freq";
    public static final String TERM_FIELD = "term";
    public static final String TERM_FIELD_NORMALIZED = "term_normalized";
    public static final String TERM_FIELD_NORMALIZED_WHITESPACE_SUBSTITUTION = "term_normalized_ws";
    public static final String FIRST_NAME_FIELD = "first_name";
    public static final String LAST_NAME_FIELD = "last_name";
    public static final String LAST_NAME_FIELD_NORMALIZED = "last_name_normalized";
    public static final String FIRST_NAME_FIELD_NORMALIZED = "first_name_normalized";
    public static final String FIRST_NAME_FIELD_ABBREVIATED = "first_name_abbreviated";

    private final String separator;
    private final Reporter reporter;
    private Directory indexDirectory;
    

    public static final PerFieldAnalyzerWrapper ANALYZER;

    static {
        Map<String, Analyzer> analyzerPerField = Maps.newHashMap();
        //analyzerPerField.put(TERM_FIELD_NORMALIZED, new Analyzer() {
        //            @Override
         //           protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
          //              //PatternReplaceCharFilter filter = new PatternReplaceCharFilter(Pattern.compile("\\s+"), " ", reader);
           //             //filter = new PatternReplaceCharFilter(Pattern.compile("([-_.:,;\\!\\?\\/\\(\\)\\+'\\&\"`´])"), "", filter);
            //            //Tokenizer tokenizer = new KeywordTokenizer(filter);
             //           Tokenizer tokenizer = new KeywordTokenizer(reader);
                        //TokenStream tok = new StandardFilter(tokenizer);
                        //tok = new LowerCaseFilter(tok);
                       // return new TokenStreamComponents(tokenizer, tok);
                   // }
               // }
        //);
        ANALYZER = new PerFieldAnalyzerWrapper(
                new SimpleAnalyzer(), analyzerPerField);
    }


    public Indexer(String separator, File indexDirectory) {
        this.separator = separator;
        reporter = new Reporter("indexed", true, 10000);
        try {
            this.indexDirectory = FSDirectory.open(indexDirectory); //TODO: move this into try block with writer? Better testability
        } catch (IOException exx) {
            throw new IllegalArgumentException("Index directory is not accessible. ", exx);
        }
    }


    public int createIndexFromFreqList(Collection<TermFrequency> termFrequencies) {

        IndexWriterConfig iwc = new IndexWriterConfig(Version.LATEST, ANALYZER);
        try (
                //Scanner scanner = new Scanner(freqlist, "UTF-8");
                IndexWriter writer = new IndexWriter(indexDirectory, iwc);
        ) {
            writer.deleteAll();

            for (TermFrequency termFrequency : termFrequencies) {
                Document doc = createDocFromTermFrequency(termFrequency);
                if (doc != null){
                    writer.addDocument(doc);
                }
                reporter.processed();
            }
            writer.commit();
            writer.forceMerge(1, true);
            reporter.done();
            return writer.numDocs();
        } catch(IOException e) {
            throw new IllegalArgumentException("Could not create index. ", e);
        }
    }


    protected Document createDocFromTermFrequency(TermFrequency termFrequency) {

        try {
            Document doc = new Document();
            String term = termFrequency.getTerm();
            if (NameMatchUtils.isFullNameCandidate(term)){

                String firstName = NameMatchUtils.getFirstName(term);
                doc.add(new StringField(FIRST_NAME_FIELD, firstName, Field.Store.YES));
                doc.add(new StringField(FIRST_NAME_FIELD_ABBREVIATED, NameMatchUtils.abbreviateFirstName(firstName), Field.Store.YES));
                doc.add(new StringField(FIRST_NAME_FIELD_NORMALIZED, normalizeErasingSpecialChars(firstName), Field.Store.YES));

                String lastName = NameMatchUtils.getLastName(term);
                doc.add(new StringField(LAST_NAME_FIELD, lastName, Field.Store.YES));
                doc.add(new StringField(LAST_NAME_FIELD_NORMALIZED, normalizeErasingSpecialChars(lastName), Field.Store.YES));
            }
            doc.add(new StringField(TERM_FIELD, term, Field.Store.YES));
            doc.add(new StringField(TERM_FIELD_NORMALIZED, normalizeErasingSpecialChars(term), Field.Store.NO));
            doc.add(new StringField(TERM_FIELD_NORMALIZED_WHITESPACE_SUBSTITUTION, normalizeErasingSpecialChars(term, true), Field.Store.NO));

            doc.add(new IntField(FREQUENCY_FIELD, termFrequency.getFrequency(), Field.Store.YES));
            return doc;
        } catch (Exception e) {
            reporter.error();
            LOGGER.warning("Could not create document from termFrequency " + termFrequency);
            return null;
        }
    }


    public static String normalizeErasingSpecialChars(String term) {
        return normalizeErasingSpecialChars(term, false);
    }

    public static String normalizeErasingSpecialChars(String term, boolean substituteByWhitespace) {
        String normalizedTerm = StringUtils.stripToEmpty(term);
        normalizedTerm = normalizedTerm.replaceAll("([-_.:,;!?/()+'&\"`´])", substituteByWhitespace ? " " : "");
        return normalize(normalizedTerm);
    }


    public static String normalize(String term) {
        String normalizedTerm = StringUtils.lowerCase(term, Locale.GERMAN);
        normalizedTerm = StringUtils.normalizeSpace(normalizedTerm);
        return normalizedTerm;
    }


}
