package com.languagetools.dictfold.vlb;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.languagetools.dictfold.ExcelFormat;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public class AbbreviationExcelInputFormat extends ExcelFormat {


    @Override
    protected Set<ColumnName> validColumns() {
        return ColumnName.validValues();
    }


    @Override
    protected AbstractColumnName columnNameOf(String columnName) {
        return ColumnName.of(columnName);
    }


    public enum ColumnName implements AbstractColumnName{
        ABBREVIATION("Abkürzung"),
        TARGET("Volle Form"),
        UNKNOWN("unknown");

        private String name;

        ColumnName(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }


        @Override
        public boolean isUnknown() {
            return this == UNKNOWN;
        }


        static ColumnName of(String name){
            ColumnName column = (ColumnName) getColumnNameFrom(name, values());
            return column != null ? column : UNKNOWN;
        }


        public static Set<ColumnName> validValues() {
            return (Set<ColumnName>) validColumnNames(values());
        }
    }


    private final static Logger LOGGER = Logger.getLogger(AbbreviationExcelInputFormat.class.getName());


    public AbbreviationExcelInputFormat(File uploadedAbbreviationFile) {
        super(uploadedAbbreviationFile);
        Preconditions.checkNotNull(inputFile);
    }


    public List<IndexSynonym> readAbbreviations() throws IOException, InvalidFormatException {
        Workbook workbook = getWorkbook();
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();
        logIfCurrentSheetIsEmpty(sheet, rowIterator);
        Row firstRow = rowIterator.next();

        Map<? extends AbstractColumnName, Integer> columnNameToIndex = getColumnNameAndCellIndex(firstRow);

        List<IndexSynonym> synonyms = Lists.newArrayList();
        while(rowIterator.hasNext()) {
            Row row = rowIterator.next();
            try {
                String abbreviation = getColumnAsString(ColumnName.ABBREVIATION, columnNameToIndex, row, false);
                String target = getColumnAsString(ColumnName.TARGET, columnNameToIndex, row, false);
                synonyms.add(IndexSynonym.of(abbreviation, target));
            } catch (Exception exx) {
                LOGGER.log(Level.SEVERE, "Ignore broken row at line number " + row.getRowNum(), exx);
            }
        }
        return synonyms;
    }

}
