package com.languagetools.dictfold.vlb;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.languagetools.dictfold.*;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

//TODO: constant for all uses ofCorrectionCandidate utf8
public class VlbCorrectionTool extends CorrectionTool {

    private final static Logger LOGGER = Logger.getLogger(VlbCorrectionTool.class.getName());
    private VlbCorrectionCandidatesMerger merger;


    public VlbCorrectionTool(Configuration configuration) throws Exception {
        super(configuration);
        merger = new VlbCorrectionCandidatesMerger();
    }


    public File addNewCorrectionsFromIndex(VlbInput vlbInput, VlbCorrectionType correctionType, String outputFileName, File oldCorrectionsFile){

        LOGGER.info("Start Vlb correction generation" );
        long startTime = System.currentTimeMillis();

        CorrectionCandidateExcelFormat correctionCandidateExcelFormat =
                new CorrectionCandidateExcelFormat(oldCorrectionsFile, new File(outputFileName));
        List<CorrectionCandidate> oldCorrectionCandidates = Lists.newArrayList();

        if (oldCorrectionsFile != null) {
            LOGGER.info(String.format(Locale.GERMANY, "File with existing corrections found: '%s' ", oldCorrectionsFile));
            oldCorrectionCandidates = readCorrectionsFromFile(correctionCandidateExcelFormat, true);
        }

        //TODO: Pass enum generationType as arg
        Collection<TermFrequency> termFrequencies = vlbInput.get(correctionType);

        List<CorrectionCandidate> newCorrectionCandidates = createCorrectionCandidates(termFrequencies);
        Collection<CorrectionCandidate> allCorrectionCandidates = merger.mergeCorrections(oldCorrectionCandidates, newCorrectionCandidates);

        new OutputProcessor(correctionCandidateExcelFormat).writeCorrectionOutput(allCorrectionCandidates);

        long endTime = System.currentTimeMillis();
        LOGGER.info("End Vlb correction... The run took " + (endTime - startTime) + " milliseconds");

        return new File(outputFileName);

    }


    private List<CorrectionCandidate> readCorrectionsFromFile(CorrectionCandidateExcelFormat correctionCandidateExcelFormat, boolean readAutomaticCorrections) {
        List<CorrectionCandidate> oldCorrectionCandidates = Lists.newArrayList();
        try {
            oldCorrectionCandidates.addAll(correctionCandidateExcelFormat.readCorrectionCandidates());
            if (readAutomaticCorrections) {
                oldCorrectionCandidates.addAll(correctionCandidateExcelFormat.readAutomaticCorrections());
            }
        } catch (Exception exx) {
            throw new IllegalArgumentException("Correction candidates file could not be read", exx);
        }
        return oldCorrectionCandidates;
    }


    public void writeCorrectionChainsInSolrSynonymFormat(Writer writer, File oldCorrectionsFile) {

        CorrectionCandidateExcelFormat correctionCandidateExcelFormat =
                new CorrectionCandidateExcelFormat(oldCorrectionsFile, null);
        Preconditions.checkNotNull(oldCorrectionsFile, "Correction input file is missing");
        Preconditions.checkNotNull(writer, "The writer is missing");

        LOGGER.info(String.format(Locale.GERMANY, "Correction file found: '%s' ", oldCorrectionsFile));

        List<CorrectionCandidate> oldCorrectionCandidates = readCorrectionsFromFile(correctionCandidateExcelFormat, false);

        CorrectionChains correctionChains = CorrectionChains.of(oldCorrectionCandidates);
        correctionChains.writeToIndexFormat(writer);

    }

}
