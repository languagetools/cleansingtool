package com.languagetools.dictfold.vlb;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Ints;
import com.languagetools.dictfold.CorrectionCandidate;
import com.languagetools.dictfold.CorrectionCandidateFormat;
import com.languagetools.dictfold.TermFrequency;

import java.util.*;
import java.util.logging.Logger;

public class OutputProcessor {//TODO: factor out an abstract output processor

    private final static Logger LOGGER = Logger.getLogger(OutputProcessor.class.getName());
    private final CorrectionCandidateFormat correctionCandidateFormat;


    public OutputProcessor(CorrectionCandidateFormat correctionCandidateFormat) {
        this.correctionCandidateFormat = correctionCandidateFormat;
        Preconditions.checkNotNull(correctionCandidateFormat);
    }

    public void writeCorrectionOutput(Collection<CorrectionCandidate> correctionCandidates) {

        List<CorrectionCandidate> correctionCandidatesToBeReviewed = Lists.newArrayList();
        List<CorrectionCandidate> automaticCorrections = Lists.newArrayList();

        separateReviewCandidatesFromAutomaticCorrections(correctionCandidates, correctionCandidatesToBeReviewed, automaticCorrections);
        writeOutput(sortCorrectionCandidates(correctionCandidatesToBeReviewed), sortAutomatiCorrections(automaticCorrections));
    }




    private void separateReviewCandidatesFromAutomaticCorrections(Collection<CorrectionCandidate> allCandidates,
                                                                  List<CorrectionCandidate> correctionCandidatesToReview,
                                                                  List<CorrectionCandidate> automaticCorrections) {

        List<CorrectionCandidate> correctionCandidatesWithNoExactMatch = Lists.newArrayList();
        List<CorrectionCandidate> exactMatches = Lists.newArrayList();
        detectExactMatches(allCandidates, correctionCandidatesWithNoExactMatch, exactMatches);

        Map<String, String> exactMatchSourceTermToTargetTerm = getExactMatchSourceTermToTargetTerm(exactMatches);

        detectAutomaticCorrectionsBasedOnTheExactMatches(correctionCandidatesWithNoExactMatch, exactMatchSourceTermToTargetTerm,
                                                         correctionCandidatesToReview, automaticCorrections);
        automaticCorrections.addAll(exactMatches);
    }


    private void detectExactMatches(Collection<CorrectionCandidate> allCandidates,
                                    List<CorrectionCandidate> correctionCandidatesWithNoExactMatch,
                                    List<CorrectionCandidate> exactMatches) {

        for (CorrectionCandidate candidate : allCandidates) {
            if ((candidate.getUsedFolders().contains("EX") || candidate.getUsedFolders().contains("EEX") )
                    && candidate.getCorrectionStatus() != CorrectionCandidate.Approved.NO){
                candidate.approve();
                exactMatches.add(candidate);
            } else {
                correctionCandidatesWithNoExactMatch.add(candidate);
            }
        }
    }


    private void detectAutomaticCorrectionsBasedOnTheExactMatches(List<CorrectionCandidate> correctionCandidatesWithNoExactMatch,
                                                                  Map<String, String> exactMatchSourceTermToTargetTerm, List<CorrectionCandidate> correctionCandidatesToBeReviewed, List<CorrectionCandidate> automaticCorrections) {

        for (CorrectionCandidate candidate : correctionCandidatesWithNoExactMatch){

            boolean addedToAutomaticCorrections = false;

            String canonicalFormOfInputCandidatesSourceTerm = exactMatchSourceTermToTargetTerm.get(candidate.getSourceTerm().getTerm());
            if (canonicalFormOfInputCandidatesSourceTerm != null) {
                addedToAutomaticCorrections =
                        checkIfADuplicateIsFoundViaTheCandidatesCanonical(candidate, canonicalFormOfInputCandidatesSourceTerm,
                                correctionCandidatesWithNoExactMatch, automaticCorrections);
            }
            if (!addedToAutomaticCorrections){
                correctionCandidatesToBeReviewed.add(candidate);
            }
        }
    }


    private boolean checkIfADuplicateIsFoundViaTheCandidatesCanonical(CorrectionCandidate inputCandidate, String canonicalFormOfInputCanidatesSourceTerm, List<CorrectionCandidate> correctionCandidates,
                                                                      List<CorrectionCandidate> automaticCorrections) {
        boolean addedToAutomaticCorrections = false;
        for (CorrectionCandidate candidate : correctionCandidates){
            if (candidate.getSourceTerm().getTerm().equals(canonicalFormOfInputCanidatesSourceTerm)
                    && candidate.getTargetTerm().getTerm().equals(inputCandidate.getTargetTerm().getTerm())){
                inputCandidate.approve();
                inputCandidate.markAsDuplicate();
                automaticCorrections.add(inputCandidate);
                addedToAutomaticCorrections = true;
                break;
            }
        }
        return addedToAutomaticCorrections;
    }


    private Map<String,String> getExactMatchSourceTermToTargetTerm(List<CorrectionCandidate> automaticCorrections) {
        Map<String, String> exactMatchSourceTermToTargetTerm = Maps.newHashMap();
        for (CorrectionCandidate correction : automaticCorrections) {
            exactMatchSourceTermToTargetTerm.put(correction.getSourceTerm().getTerm(), correction.getTargetTerm().getTerm());
        }
        return exactMatchSourceTermToTargetTerm;
    }


    private List<CorrectionCandidate> sortCorrectionCandidates(List<CorrectionCandidate> canidatesToSort) {
        return Ordering.from(new AlphabeticCandidateComparator()).immutableSortedCopy(canidatesToSort);
    }

    private List<CorrectionCandidate> sortAutomatiCorrections(List<CorrectionCandidate> canidatesToSort) {
        return Ordering.from(new AutomaticCorrectionsComparator()).immutableSortedCopy(canidatesToSort);
    }


   //TODO: how to share header (column name information from input to output file)
    private void writeOutput(List<CorrectionCandidate> correctionCandidates, List<CorrectionCandidate> automaticCorrections) {
        LOGGER.info(String.format(Locale.GERMANY, "Write %s correction candidates to output ", correctionCandidates.size()));
        LOGGER.info(String.format(Locale.GERMANY, "Write %s automatic correction to output ", automaticCorrections.size()));
        correctionCandidateFormat.writeCandidates(correctionCandidates, automaticCorrections);
    }


    private class AlphabeticCandidateComparator implements Comparator<CorrectionCandidate> {

            @Override
            public int compare(CorrectionCandidate a, CorrectionCandidate b) {
                TermFrequency aSourceTermFrequency = a.getSourceTerm();
                TermFrequency bSourceTermFrequency = b.getSourceTerm();

                int sourceTermCompared = aSourceTermFrequency.getTerm().compareTo(bSourceTermFrequency.getTerm());
                if (sourceTermCompared != 0){
                    return sourceTermCompared;
                }

                int sourceFrequencyCompared = Ints.compare(bSourceTermFrequency.getFrequency(), aSourceTermFrequency.getFrequency());
                if (sourceFrequencyCompared != 0) {
                    return sourceFrequencyCompared;
                }

                return a.getTargetTerm().getTerm().compareTo(b.getTargetTerm().getTerm());
            }
    }


    private class AutomaticCorrectionsComparator extends AlphabeticCandidateComparator {

        @Override
        public int compare(CorrectionCandidate a, CorrectionCandidate b) {
            int commentsCompared = a.getComment().compareTo(b.getComment());
            if (commentsCompared != 0) {
                return commentsCompared;
            }
            return super.compare(a, b);
        }
    }

}
