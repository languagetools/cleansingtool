package com.languagetools.dictfold.vlb;


import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.languagetools.dictfold.TermFrequency;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;

import java.util.Collection;
import java.util.EnumMap;
import java.util.List;

public class VlbSolrSearcher implements VlbInput {

    private final String searchUrl;
    private static EnumMap<VlbCorrectionType, String> typeToFacet;
    static {
        typeToFacet = Maps.newEnumMap(VlbCorrectionType.class);
        typeToFacet.put(VlbCorrectionType.AUTHORS, "authors_facet");
        typeToFacet.put(VlbCorrectionType.TAGS, "keywords_facet");
        typeToFacet.put(VlbCorrectionType.DNB_TAGS, "keywordsDnb_facet");
        typeToFacet.put(VlbCorrectionType.PUBLISHER, "publisher_facet");
    }


    public VlbSolrSearcher(String searchUrl) {
        this.searchUrl = searchUrl;
    }

    @Override
    public Collection<TermFrequency> get(VlbCorrectionType correctionType) {
        String facetField = typeToFacet.get(correctionType); //TODO: default for non existing key?
        Collection<TermFrequency> termFrequencies = getTermFrequenciesFrom(facetField);
        return termFrequencies;
    }


    private Collection<TermFrequency> getTermFrequenciesFrom(String facetField) {
        QueryResponse response = null;
        try {
            response = getResponseFromFacet(facetField);
        } catch (SolrServerException e) {
            throw new IllegalStateException("An Error occured while querying solr for facet field " + facetField, e);
        }
        return convertToTermFrequency(response, facetField);
    }


    private QueryResponse getResponseFromFacet(String facetField) throws SolrServerException {
        SolrServer server = new HttpSolrServer(searchUrl);
        SolrQuery solrQuery = new SolrQuery()
                .setQuery("*:*")
                .setFacet(true)
                .setFacetMinCount(1)
                .setFacetLimit(-1)
                .addFacetField(facetField);
        return server.query(solrQuery);
    }


    private Collection<TermFrequency> convertToTermFrequency(QueryResponse response, String facetFieldName) {
        List<TermFrequency> termFrequencies = Lists.newArrayList();
        FacetField facetField = response.getFacetField(facetFieldName);
        if (facetField != null) {
            for (FacetField.Count count : facetField.getValues()) {
                TermFrequency termFrequency = TermFrequency.of(count.getName(), (int) count.getCount());
                termFrequencies.add(termFrequency);
            }
        }
        return termFrequencies;
    }
}