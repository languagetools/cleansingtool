package com.languagetools.dictfold.vlb;


import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.languagetools.dictfold.ExcelFormat;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SynonymExcelInputFormat extends ExcelFormat {


    @Override
    protected Set<ColumnName> validColumns() {
        return ColumnName.validValues();
    }


    @Override
    protected AbstractColumnName columnNameOf(String columnName) {
        return ColumnName.of(columnName);
    }


    public enum ColumnName implements AbstractColumnName{
        SYNONYM_SOURCE("Wort 1"),
        SYNONYM_TARGET("Wort 2"),
        ALL_COLUMNS("Alle Bereiche"),
        DESCRIPTION("Freitexte"),
        WGS("WGS"),
        TITLE("Thema"),
        DNB_TAGS("DNB-Schlagworte"),
        PUBLISHER_TAGS("Verlagsschlagworte"),
        UNKNOWN("unknown");

        private String name;

        ColumnName(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }


        @Override
        public boolean isUnknown() {
            return this == UNKNOWN;
        }


        public boolean isValidSynonymType(){
            return this != ColumnName.SYNONYM_SOURCE && this != SYNONYM_TARGET && this != UNKNOWN && this != ALL_COLUMNS;
        }


        static ColumnName of(String name){
            ColumnName column = (ColumnName) getColumnNameFrom(name, values());
            return column != null ? column : UNKNOWN;
        }


        public static Set<ColumnName> validValues() {
            return (Set<ColumnName>) validColumnNames(values());
        }
    }


    private final static Logger LOGGER = Logger.getLogger(SynonymExcelInputFormat.class.getName());


    public SynonymExcelInputFormat(File inputFile) {
        super(inputFile);
        Preconditions.checkNotNull(inputFile);
    }

    public List<IndexSynonym> readSynonymsOfType(ColumnName columnName) throws IOException, InvalidFormatException {
        Preconditions.checkNotNull(columnName);
        Preconditions.checkArgument(columnName.isValidSynonymType(), "The requested column name has to specify an valid synonym type");
        Workbook workbook = getWorkbook();
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();
        logIfCurrentSheetIsEmpty(sheet, rowIterator);
        Row firstRow = rowIterator.next();

        Map<? extends AbstractColumnName, Integer> columnNameToIndex = getColumnNameAndCellIndex(firstRow);

        List<IndexSynonym> synonyms = Lists.newArrayList();
        while(rowIterator.hasNext()) {
            Row row = rowIterator.next();
            try {

                if (!cellIsFilled(columnName, columnNameToIndex, row) &&
                    !cellIsFilled(ColumnName.ALL_COLUMNS, columnNameToIndex, row)) {
                    continue;
                }

                String synonymSourceTerm = getColumnAsString(ColumnName.SYNONYM_SOURCE, columnNameToIndex, row, false);
                String synonymTargetTerm = getColumnAsString(ColumnName.SYNONYM_TARGET, columnNameToIndex, row, false);
                synonyms.add(IndexSynonym.of(synonymSourceTerm, synonymTargetTerm));
            } catch (Exception exx) {
                LOGGER.log(Level.SEVERE, "Ignore broken row at line number " + row.getRowNum(), exx);
            }
        }
        return synonyms;
    }


    private boolean cellIsFilled(ColumnName columnName, Map<? extends AbstractColumnName, Integer> columnNameToIndex, Row row) {
        Cell allColumnsCell = getCellForColumnName(columnName, columnNameToIndex, row);
        if (allColumnsCell != null && StringUtils.isNotBlank(allColumnsCell.getStringCellValue())) {
            return true;
        }
        return false;
    }

}
