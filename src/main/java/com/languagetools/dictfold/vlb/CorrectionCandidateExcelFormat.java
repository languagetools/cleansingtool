package com.languagetools.dictfold.vlb;

import com.google.common.collect.Lists;
import com.languagetools.dictfold.CorrectionCandidate;
import com.languagetools.dictfold.CorrectionCandidateFormat;
import com.languagetools.dictfold.ExcelFormat;
import com.languagetools.dictfold.TermFrequency;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;



//TODO: encoding == utf8 ??
public class CorrectionCandidateExcelFormat extends ExcelFormat implements CorrectionCandidateFormat {

    @Override
    protected Set<? extends AbstractColumnName> validColumns() {
        return ColumnName.validValues();
    }


    @Override
    protected AbstractColumnName columnNameOf(String columnName) {
        return ColumnName.of(columnName);
    }


    enum ColumnName implements AbstractColumnName{
        SOURCE_TERM("Term1"),
        SOURCE_FREQUENCY("Frequenz1"),
        TARGET_TERM("Term2"),
        TARGET_FREQUENCY("Frequenz2"),
        STATUS("Status"),
        COMMENT("Kommentar"),
        UNKNOWN("unknown");

        private String name;

        ColumnName(String name) {
            this.name = name;
        }


        @Override
        public String getName() {
            return name;
        }

        @Override
        public boolean isUnknown() {
            return this == UNKNOWN;
        }

        static ColumnName of(String name){
            ColumnName column = (ColumnName) getColumnNameFrom(name, values());
            return column != null ? column : UNKNOWN;
        }


        public static Set<ColumnName> validValues() {
            return (Set<ColumnName>) validColumnNames(values());
        }
    }


    private static String CORRECTION_SHEET = "Korrekturvorschläge";
    private static String AUTOMATIC_CORRECTION_SHEET = "Automatische Korrekturen";

    private final static Logger LOGGER = Logger.getLogger(CorrectionCandidateExcelFormat.class.getName());
    private final File outputFile;


    public CorrectionCandidateExcelFormat(File inputFile, File outputFile) {
       super(inputFile);
        this.outputFile = outputFile;
    }

    @Override
    public List<CorrectionCandidate> readCorrectionCandidates() throws Exception {
        return readSheetFromCandidateFile(CORRECTION_SHEET);
    }

    @Override
    public List<CorrectionCandidate> readAutomaticCorrections() throws Exception {
        return readSheetFromCandidateFile(AUTOMATIC_CORRECTION_SHEET);
    }

    private List<CorrectionCandidate> readSheetFromCandidateFile(String sheetName) throws IOException, InvalidFormatException {
        List<CorrectionCandidate> correctionCandidates = Lists.newArrayList();

        XSSFWorkbook workbook = new XSSFWorkbook(inputFile); //TODO get Workbook
        XSSFSheet sheet = workbook.getSheet(sheetName);

        Iterator<Row> rowIterator = sheet.iterator();
        logIfCurrentSheetIsEmpty(sheet, rowIterator);
        Row firstRow = rowIterator.next();
        Map<? extends AbstractColumnName, Integer> columnNameToIndex = getColumnNameAndCellIndex(firstRow);

        while(rowIterator.hasNext()) {
            Row row = rowIterator.next();
            try {
                TermFrequency sourceTermFrequency = createTermFrequency(ColumnName.SOURCE_FREQUENCY, ColumnName.SOURCE_TERM, columnNameToIndex, row);
                TermFrequency targetTermFrequency = createTermFrequency(ColumnName.TARGET_FREQUENCY, ColumnName.TARGET_TERM, columnNameToIndex, row);
                String correctionStatus = getColumnAsString(ColumnName.STATUS, columnNameToIndex, row, true);
                String comment = StringUtils.trimToEmpty(getColumnAsString(ColumnName.COMMENT, columnNameToIndex, row, true));

                correctionCandidates.add(CorrectionCandidate.of(sourceTermFrequency, targetTermFrequency, CorrectionCandidate.Approved.of(correctionStatus), comment));
            } catch (Exception exx) {
                LOGGER.log(Level.SEVERE, "Ignore broken row at line number " + row.getRowNum(), exx);
            }
        }
        return correctionCandidates;
    }



    public TermFrequency createTermFrequency(ColumnName frequencyColumnName, ColumnName termColumnName, Map<? extends AbstractColumnName, Integer> columnNameToIndex, Row row) {

        String term = getColumnAsString(termColumnName, columnNameToIndex, row, false);
        Integer frequency = getColumnAsInt(frequencyColumnName, columnNameToIndex, row);

        return  TermFrequency.of(term, frequency);
    }


    //TODO: , Charset.forName("utf-8")
    public void writeCandidates(List<CorrectionCandidate> corrections, List<CorrectionCandidate> automaticCorrections) {

        try {
            FileOutputStream fileOut = new FileOutputStream(outputFile);
            XSSFWorkbook workbook = new XSSFWorkbook();

            writeCorrectionsToWorkSheet(workbook, CORRECTION_SHEET, corrections);
            writeCorrectionsToWorkSheet(workbook, AUTOMATIC_CORRECTION_SHEET, automaticCorrections);

            workbook.write(fileOut);
            fileOut.flush();
            fileOut.close();
        } catch (Exception e) {
            throw new IllegalStateException("Error occured while writing candidates to excel file", e);
        }
    }


    public void writeCorrectionsToWorkSheet(XSSFWorkbook workbook, String workSheetName, List<CorrectionCandidate> corrections) {
        XSSFSheet worksheet = workbook.createSheet(workSheetName);

        XSSFRow firstRow = worksheet.createRow(0);
        writeCell(firstRow, 0, ColumnName.SOURCE_TERM.getName());
        writeCell(firstRow, 1, ColumnName.SOURCE_FREQUENCY.getName());
        writeCell(firstRow, 2, ColumnName.TARGET_TERM.getName());
        writeCell(firstRow, 3, ColumnName.TARGET_FREQUENCY.getName());
        writeCell(firstRow, 4, ColumnName.STATUS.getName());
        writeCell(firstRow, 5, ColumnName.COMMENT.getName());

        int rowNumber = 1;
        for (CorrectionCandidate correction : corrections) {
            XSSFRow row = worksheet.createRow(rowNumber);
            rowNumber++;

            TermFrequency sourceTermFrequency = correction.getSourceTerm();
            writeCell(row, 0, sourceTermFrequency.getTerm());
            writeCell(row, 1, sourceTermFrequency.getFrequency());

            TermFrequency targetTermFrequency = correction.getTargetTerm();
            writeCell(row, 2, targetTermFrequency.getTerm());
            writeCell(row, 3, targetTermFrequency.getFrequency());
            writeCell(row, 4, correction.getCorrectionStatus().toString());
            writeCell(row, 5, correction.getComment());
            //writeCell(row, 5, correction.getUsedFolders().toString());
        }
    }


    public void writeCell(XSSFRow row, int cellIndex, String cellValue) {
        XSSFCell cell = row.createCell(cellIndex);
        cell.setCellValue(cellValue);
    }

    public void writeCell(XSSFRow row, int cellIndex, int cellValue) {
        XSSFCell cell = row.createCell(cellIndex);
        cell.setCellValue(cellValue);
    }
}
