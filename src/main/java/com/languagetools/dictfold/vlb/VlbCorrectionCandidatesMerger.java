package com.languagetools.dictfold.vlb;

import com.google.common.base.Preconditions;
import com.google.common.collect.*;
import com.languagetools.dictfold.CorrectionCandidate;
import com.languagetools.dictfold.TermFrequency;

import java.util.*;
import java.util.logging.Logger;

public class VlbCorrectionCandidatesMerger {


    private final static Logger LOGGER = Logger.getLogger(VlbCorrectionCandidatesMerger.class.getName());


    public Collection<CorrectionCandidate> mergeCorrections(Collection<CorrectionCandidate> oldCorrectionCandidates, Collection<CorrectionCandidate> newCorrectionCandidates) {
        Preconditions.checkNotNull(oldCorrectionCandidates);
        Preconditions.checkNotNull(newCorrectionCandidates);

        Set<CorrectionCandidate> mergedCandidates = Sets.newHashSet();
        Map<String, CorrectionCandidate> termToOldCandidate = createTermToCandidate(oldCorrectionCandidates);
        Map<String, CorrectionCandidate> termToNewCandidate = createTermToCandidate(newCorrectionCandidates);

        for (Map.Entry<String, CorrectionCandidate> newCorrectionCandidateEntry : termToNewCandidate.entrySet()){
            CorrectionCandidate newCorrectionCandidate = newCorrectionCandidateEntry.getValue();
            CorrectionCandidate oldCorrectionCandidate = termToOldCandidate.get(newCorrectionCandidateEntry.getKey());
            if (oldCorrectionCandidate == null) {
                addUnmatchedNewCandidate(newCorrectionCandidate, mergedCandidates);
            } else {
                mergedCandidates.add(mergeCandidate(newCorrectionCandidate, oldCorrectionCandidate));
            }
        }

        for (Map.Entry<String, CorrectionCandidate> oldCorrectionCandidateEntry : termToOldCandidate.entrySet()){
            CorrectionCandidate oldCorrectionCandidate = oldCorrectionCandidateEntry.getValue();
            CorrectionCandidate newCorrectionCandidate = termToNewCandidate.get(oldCorrectionCandidateEntry.getKey());
            if (newCorrectionCandidate == null) {
                addOldUnmatchedCandidate(oldCorrectionCandidate, mergedCandidates);
            } 
        }

        return mergedCandidates;
    }


    private void addOldUnmatchedCandidate(CorrectionCandidate oldCorrectionCandidate, Set<CorrectionCandidate> mergedCandidates) {
        CorrectionCandidate candidateWithZeroFrequencies = CorrectionCandidate.of(TermFrequency.of(oldCorrectionCandidate.getSourceTerm().getTerm(), 0), TermFrequency.of(oldCorrectionCandidate.getTargetTerm().getTerm(), 0),
                oldCorrectionCandidate.getCorrectionStatus(), oldCorrectionCandidate.getComment(), Iterables.toArray(oldCorrectionCandidate.getUsedFolders(), String.class));
        mergedCandidates.add(candidateWithZeroFrequencies);
    }


    private CorrectionCandidate mergeCandidate(CorrectionCandidate newCorrectionCandidate, CorrectionCandidate oldCorrectionCandidate) {
        CorrectionCandidate.Approved correctionStatus = oldCorrectionCandidate.getCorrectionStatus() == CorrectionCandidate.Approved.OPEN ?
                    newCorrectionCandidate.getCorrectionStatus() : oldCorrectionCandidate.getCorrectionStatus();

        return CorrectionCandidate.of(newCorrectionCandidate.getSourceTerm(), newCorrectionCandidate.getTargetTerm(),
                correctionStatus, oldCorrectionCandidate.getComment(),
                mergeUsedFolders(oldCorrectionCandidate, newCorrectionCandidate));
    }


    private String[] mergeUsedFolders(CorrectionCandidate oldCandidate, CorrectionCandidate newCandidate) {
        Set<String> mergedUsedFolders = Sets.newHashSet();
        mergedUsedFolders.addAll(oldCandidate.getUsedFolders());
        mergedUsedFolders.addAll(newCandidate.getUsedFolders());
        return Iterables.toArray(mergedUsedFolders, String.class);
    }


    private void addUnmatchedNewCandidate(CorrectionCandidate newCorrectionCandidate, Set<CorrectionCandidate> mergedCandidates) {
        mergedCandidates.add(newCorrectionCandidate);
    }


    private Map<String, CorrectionCandidate> createTermToCandidate(Collection<CorrectionCandidate> candidates){

        Map<String, CorrectionCandidate> termsToCandidate  = Maps.newHashMap();

        for (CorrectionCandidate candidate : candidates) {
            termsToCandidate.put(createSkipKey(candidate), candidate);
        }
        return termsToCandidate;
    }


    private String createSkipKey(CorrectionCandidate candidate) {
        return candidate.getSourceTerm().getTerm() + candidate.getTargetTerm().getTerm();
    }



}
