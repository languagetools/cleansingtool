package com.languagetools.dictfold.vlb;


import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class IndexSynonym {

    private String sourceTerm;
    private String targetTerm;


    private IndexSynonym(String sourceTerm, String targetTerm) {
        this.sourceTerm = sourceTerm;
        this.targetTerm = targetTerm;
    }


    public static IndexSynonym of(String sourceTerm, String targetTerm) {
        Preconditions.checkArgument(StringUtils.isNotBlank(sourceTerm));
        Preconditions.checkArgument(StringUtils.isNotBlank(targetTerm));
        return new IndexSynonym(sourceTerm, targetTerm);
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }


    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }


    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }


    public String getSourceTerm() {
        return sourceTerm;
    }


    public String getTargetTerm() {
        return targetTerm;
    }
}
