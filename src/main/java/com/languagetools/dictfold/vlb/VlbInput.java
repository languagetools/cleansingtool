package com.languagetools.dictfold.vlb;


import com.languagetools.dictfold.TermFrequency;

import java.util.Collection;

public interface VlbInput {

    Collection<TermFrequency> get(VlbCorrectionType correctionType);
}
