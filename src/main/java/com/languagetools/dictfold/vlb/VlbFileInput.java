package com.languagetools.dictfold.vlb;


import com.languagetools.dictfold.TermFrequency;

import java.io.File;
import java.util.Collection;

public class VlbFileInput implements  VlbInput{

    private final File authorsInputFile;
    private final String separator;


    public VlbFileInput(File authorsInputFile, String separator) {
        this.authorsInputFile = authorsInputFile;
        this.separator = separator;
    }


    @Override
    public Collection<TermFrequency> get(VlbCorrectionType correctionType) {
        if (correctionType == VlbCorrectionType.AUTHORS) {
            return TermFrequency.getTermFrequenciesFromFile(authorsInputFile, separator);
        }
        return null;
    }
}
