/*
 * Copyright (c) 2015 by ideenplanet GmbH. All rights reserved.
 */

package com.languagetools.dictfold.vlb.web;

import com.languagetools.dictfold.vlb.VlbCorrectionType;
import com.languagetools.dictfold.vlb.VlbCorrectionTool;
import com.languagetools.dictfold.vlb.VlbSolrSearcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class CleansingService extends AbstractVlbService {

    private final static Logger LOGGER = Logger.getLogger(CleansingService.class.getName());
    private static final String INDEX_CORRECTIONS_TEXT_FILENAME_AUTHOR = "corrections_%s.txt";

    @Autowired
    private VlbSolrSearcher solrSearcher;

    @Autowired
    private VlbCorrectionTool vlbCorrectionTool;


    @Async
    public Future<File> createCorrectionFile(String outputFileName, VlbCorrectionType correctionType, File existingCorrectionFile) {
        File file = null;
        try {
            runStatus.set(1);
            file = vlbCorrectionTool.addNewCorrectionsFromIndex(solrSearcher, correctionType,
                                                                outputFileName, existingCorrectionFile);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Could not create corrections index (" + correctionType.toString() + ")", e);
        } finally {
            runStatus.set(0);
        }
        return new AsyncResult<>(file);
    }


    @Async
    public Future<File> createCorrectionFile(String outputFileName, VlbCorrectionType correctionType) {
        return createCorrectionFile(outputFileName, correctionType, null);
    }


    @Async
    public Future<Boolean> applyCorrectionsToIndex(File uploadedCorrectionFile, VlbCorrectionType correctionType) throws IOException {
        boolean success = false;
        try {
            runStatus.set(1);
            String outputFileName = inOutputDirectory(String.format(INDEX_CORRECTIONS_TEXT_FILENAME_AUTHOR,
                                                                    correctionType.toString().toLowerCase()));
            FileWriter writer = new FileWriter(new File(outputFileName));
            vlbCorrectionTool.writeCorrectionChainsInSolrSynonymFormat(writer, uploadedCorrectionFile);
            Path sourcePath = Paths.get(outputFileName);
            Files.copy(sourcePath, Paths.get(solrCoreDirectory).resolve(sourcePath.getFileName()), StandardCopyOption.REPLACE_EXISTING);
            success = true;
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Could not apply corrections to index (" + correctionType.toString() + ")" , e);
        } finally {
            runStatus.set(0);
        }
        return new AsyncResult<>(success);
    }

}
