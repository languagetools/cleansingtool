package com.languagetools.dictfold.vlb.web;

import com.languagetools.dictfold.vlb.VlbCorrectionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;


@Controller
@RequestMapping("/cleansing/v1")
public class CleansingController extends AbstractController {


    private static Logger LOGGER = Logger.getLogger(CleansingController.class.getName());
    private static final String CORRECTION_OUTPUT_FILENAME = "output_%s.xlsx"; //TODO add marker if input file was provided?
    private static final String UPLOADED_CORRECTIONS_TO_APPLY_EXCEL_FILENAME = "uploaded_%s_Corrections_to_apply.xlsx";
    private static final String UPLOADED_CORRECTIONS_TO_CREATE_EXCEL_FILENAME = "uploaded_%s_Corrections.xlsx";


    @Autowired
    private CleansingService cleansingService;


    @RequestMapping(value = "{type}")
    public String showCleansingOptionsForType(@PathVariable VlbCorrectionType type, Model model) {
        addCorrectionTypeToModel(type, model);
        return "cleansing";
    }


    @RequestMapping(value = "apply/{type}", method= RequestMethod.POST)
    public String applyCorrections(@PathVariable VlbCorrectionType type,
                                   @RequestParam("file") MultipartFile correctionMultiFileToUpload,
                                   HttpSession session, Model model) {

        throwIfAnotherProcessIsRunningYet();

        if (!correctionMultiFileToUpload.isEmpty()) {
            try {
                File uploadedCorrectionFile = upload(correctionMultiFileToUpload, filename(UPLOADED_CORRECTIONS_TO_APPLY_EXCEL_FILENAME, type));
                backupFileUploadedFile(uploadedCorrectionFile);
                Future<Boolean> success = cleansingService.applyCorrectionsToIndex(uploadedCorrectionFile, type);
                session.setAttribute("success", success);
                addCorrectionTypeToModel(type, model);
                return "corrections_applied";
            } catch (Exception exx) {
                LOGGER.log(Level.SEVERE, "Error while uploading correction file", exx);
                throw new UploadError();
            }
        } else {
            throw new FileMissingError();
        }
    }


    private String filename(String uploadedCorrectionsToApplyExcelFilename, VlbCorrectionType type) {
        return String.format(uploadedCorrectionsToApplyExcelFilename, type.toString().toLowerCase());
    }


    private void throwIfAnotherProcessIsRunningYet() {
        if (cleansingService.isProcessRunning()) {
            throw new ProcessYetRunningError();
        }
    }


    @RequestMapping(value = "create/{type}", method= RequestMethod.POST)
    public String createCorrections(@PathVariable VlbCorrectionType type,
                                    @RequestParam("file") MultipartFile correctionMultiFileToUpload,
                                    @RequestParam(value="no_file_provided", defaultValue="false") boolean noExistingCorrectionsAreProvided,
                                    Model model) throws Exception {

        Future<File> correctionFileToDownload = null;
        throwIfAnotherProcessIsRunningYet();
        if (noExistingCorrectionsAreProvided) {
            correctionFileToDownload = cleansingService.createCorrectionFile(pathInOutputDirectory(filename(CORRECTION_OUTPUT_FILENAME, type)), type);
        } else if (!correctionMultiFileToUpload.isEmpty()) {
            File uploadedCorrectionFile = null;
            try {
                uploadedCorrectionFile = upload(correctionMultiFileToUpload, filename(UPLOADED_CORRECTIONS_TO_CREATE_EXCEL_FILENAME, type));
                backupFileUploadedFile(uploadedCorrectionFile);
            } catch (Exception exx) {
                LOGGER.log(Level.SEVERE, "Error while uploading correction file", exx);
                throw new UploadError();
            }
            correctionFileToDownload =  cleansingService.createCorrectionFile(pathInOutputDirectory(filename(CORRECTION_OUTPUT_FILENAME, type)), type, uploadedCorrectionFile);
        } else {
            throw new NoFileOptionSelectedError();
        }
        addCorrectionTypeToModel(type, model);
        return "correction_started";
    }


    @RequestMapping(value = "request_correction_download/{type}", method= RequestMethod.GET)
    public String requestCorrectionsDownload(@PathVariable("type") VlbCorrectionType type, Model model) {
        throwIfAnotherProcessIsRunningYet();
        File fileToDownload = fileInOutputDirectory(filename(CORRECTION_OUTPUT_FILENAME, type));
        model.addAttribute("downloadFile", fileToDownload);
        addCorrectionTypeToModel(type, model);
        return "correction_started";
    }


    @RequestMapping(value = "download_corrections",
            method= RequestMethod.GET,
            produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @ResponseBody
    public FileSystemResource downloadCorrections(@RequestParam("file") String fileName) {
        return new FileSystemResource(fileName);
    }


    @RequestMapping(value = "create_upload/{type}")
    public String goToUploadViewForCorrectionCreation(@PathVariable VlbCorrectionType type, Model model) {
        addCorrectionTypeToModel(type, model);
        return "create_correction_upload";
    }


    @RequestMapping(value = "apply_upload/{type}")
    public String goToUploadViewForIndexApplication(@PathVariable VlbCorrectionType type, Model model) {
        addCorrectionTypeToModel(type, model);
        return "apply_correction_upload";
    }

}
