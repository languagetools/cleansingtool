package com.languagetools.dictfold.vlb.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@RequestMapping("/abbreviations/v1")
public class AbbreviationController extends AbstractController {


    private static Logger LOGGER = Logger.getLogger(AbbreviationController.class.getName());

    private static final String UPLOADED_ABBREVIATIONS_EXCEL_FILENAME_XLSX = "uploaded_abbreviations.xlsx";
    private static final String UPLOADED_ABBREVIATIONS_EXCEL_FILENAME_XLS = "uploaded_abbreviations.xls";

    @Autowired
    private AbbreviationsService abbreviationsService;

    @RequestMapping(value = "upload_abbreviations")
    public String uploadSynonyms() {
        return "abbreviations_upload";
    }

    @RequestMapping(value = "/")
    public String showSynonyms() {
        return "abbreviations";
    }

    @RequestMapping(value = "abbreviations_uploaded", method= RequestMethod.POST)
    public String synonymsUploaded(@RequestParam("file") MultipartFile correctionMultiFileToUpload, HttpSession session) {

        throwIfAnotherProcessIsRunningYet();

        if (!correctionMultiFileToUpload.isEmpty()) {
            try {
                String localAbbreviationOutputName = correctionMultiFileToUpload.getOriginalFilename().endsWith(".xlsx")  //TODO instead of this and code in syn excelformat => try catch logic there to try both formats
                        ? UPLOADED_ABBREVIATIONS_EXCEL_FILENAME_XLSX : UPLOADED_ABBREVIATIONS_EXCEL_FILENAME_XLS;

                File uploadedAbbreviationsFile = upload(correctionMultiFileToUpload, localAbbreviationOutputName);
                backupFileUploadedFile(uploadedAbbreviationsFile);
                Future<Boolean> success = abbreviationsService.applyAbbreviationsToIndex(uploadedAbbreviationsFile);
                session.setAttribute("success", success);
                return "abbreviations_applied";
            } catch (Exception exx) {
                LOGGER.log(Level.SEVERE, "Error while uploading abbreviation file", exx);
                throw new UploadError();
            }
        } else {
            throw new FileMissingError();
        }
    }


    private void throwIfAnotherProcessIsRunningYet() {
        if (abbreviationsService.isProcessRunning()) {
            throw new ProcessYetRunningError();
        }

    }


}
