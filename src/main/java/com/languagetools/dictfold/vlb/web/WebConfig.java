package com.languagetools.dictfold.vlb.web;

import com.languagetools.dictfold.vlb.VlbCorrectionTool;
import com.languagetools.dictfold.vlb.VlbSolrSearcher;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.resource.ResourceUrlEncodingFilter;
import org.springframework.web.servlet.resource.VersionResourceResolver;

import java.util.Locale;

@EnableWebMvc
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    @Value("${solr.search_url}")
    private String solrSearchUrl;


    @Bean
    public PropertiesConfiguration properties() throws ConfigurationException {
        return new PropertiesConfiguration("config.properties");
    }


    @Bean
    public VlbCorrectionTool tool() throws Exception {
        return new VlbCorrectionTool(properties());
    }


    @Bean
    public ResourceUrlEncodingFilter resourceUrlEncodingFilter() {
        return new ResourceUrlEncodingFilter();
    }


    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.GERMANY);
        return slr;
    }


    @Bean
    public VlbSolrSearcher solrSearcher() {
        return new VlbSolrSearcher(solrSearchUrl);
    }


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("**/*.js", "**/*.css", "**/*.jpg", "**/*.png")
                .addResourceLocations("classpath:/static/")
                .resourceChain(true)
                .addResolver(
                        new VersionResourceResolver()
                                .addContentVersionStrategy("/assets/**")
                );
    }
}