package com.languagetools.dictfold.vlb.web;


import com.languagetools.dictfold.vlb.IndexSynonym;
import com.languagetools.dictfold.vlb.SynonymExcelInputFormat;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class SynonymService extends AbstractVlbSynonymService {


    private final static Logger LOGGER = Logger.getLogger(SynonymService.class.getName());

    private static final String SYNONYMS_TITLE_FILENAME = "synonyms_title.txt";
    private static final String SYNONYMS_DESCRIPTION_FILENAME = "synonyms_description.txt";
    private static final String SYNONYMS_WGS_FILENAME = "synonyms_wgs.txt";
    private static final String SYNONYMS_DNB_TAGS_FILENAME = "synonyms_dnb_tags.txt";
    private static final String SYNONYMS_PUBLISHER_TAGS_FILENAME = "synonyms_publisher_tags.txt";


    @Async
    public Future<Boolean> applySynonymsToIndex(File uploadedSynonymsFile) throws IOException {
        boolean success = false;
        try {
            runStatus.set(1);
            SynonymExcelInputFormat synonymExcelInputFormat = new SynonymExcelInputFormat(uploadedSynonymsFile);
            applySynonymsInExcelColumn(SynonymExcelInputFormat.ColumnName.TITLE, SYNONYMS_TITLE_FILENAME, synonymExcelInputFormat);
            applySynonymsInExcelColumn(SynonymExcelInputFormat.ColumnName.DESCRIPTION, SYNONYMS_DESCRIPTION_FILENAME, synonymExcelInputFormat);
            applySynonymsInExcelColumn(SynonymExcelInputFormat.ColumnName.WGS, SYNONYMS_WGS_FILENAME, synonymExcelInputFormat);
            applySynonymsInExcelColumn(SynonymExcelInputFormat.ColumnName.DNB_TAGS, SYNONYMS_DNB_TAGS_FILENAME, synonymExcelInputFormat);
            applySynonymsInExcelColumn(SynonymExcelInputFormat.ColumnName.PUBLISHER_TAGS, SYNONYMS_PUBLISHER_TAGS_FILENAME, synonymExcelInputFormat);
            success = true;
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Could not apply synonyms to index", e);
        } finally {
            runStatus.set(0);
        }
        return new AsyncResult<>(success);
    }


    private void applySynonymsInExcelColumn(SynonymExcelInputFormat.ColumnName columnName, String outputFileName, SynonymExcelInputFormat synonymExcelInputFormat) throws Exception {
        List<IndexSynonym> synonyms = synonymExcelInputFormat.readSynonymsOfType(columnName);
        applySynonyms(outputFileName, synonyms);
    }



}
