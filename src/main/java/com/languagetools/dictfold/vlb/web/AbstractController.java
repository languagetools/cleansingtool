package com.languagetools.dictfold.vlb.web;


import com.languagetools.dictfold.vlb.VlbCorrectionType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AbstractController {

    @Value("${output_directory}")
    protected String outputDirectory;


    protected String pathInOutputDirectory(String fileName) {
        return fileInOutputDirectory(fileName).getAbsolutePath();
    }

    protected File fileInOutputDirectory(String fileName) {
        return new File(outputDirectory, fileName);
    }


    protected File upload(MultipartFile uploadedCorrectionMultiFile, String localFileName) throws IOException {
        byte[] bytes = uploadedCorrectionMultiFile.getBytes();
        File uploadedCorrectionFile = new File(outputDirectory, localFileName);
        BufferedOutputStream stream =
                new BufferedOutputStream(new FileOutputStream(uploadedCorrectionFile));
        stream.write(bytes);
        stream.close();
        return uploadedCorrectionFile;
    }


    protected void backupFileUploadedFile(File uploadedCorrectionFile) throws IOException {
        String date = new SimpleDateFormat("yyyy-MM-dd_hh_mm'.txt'").format(new Date());
        String uploadedCorrectionFileName = uploadedCorrectionFile.getAbsolutePath();
        String backupFileName = uploadedCorrectionFileName + "_" + date;
        Files.copy(Paths.get(uploadedCorrectionFileName), Paths.get(backupFileName), StandardCopyOption.REPLACE_EXISTING);
    }


    protected void addCorrectionTypeToModel(VlbCorrectionType type, Model model) {
        model.addAttribute("correctionType", type.toString());
        model.addAttribute("correctionTypeToDisplay", type.getUserString());
    }


    @ExceptionHandler({Exception.class})
    public ModelAndView handleError(Exception e){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("reason", e.getMessage());
        modelAndView.setViewName("error");
        return modelAndView;
    }


    static class UploadError extends RuntimeException {
        UploadError() {
            super("Beim Hochladen der Datei ist ein Fehler aufgetreten! Bitte versuchen Sie erneut eine Datei hochzuladen.");
        }
    }


    static class FileMissingError extends RuntimeException {
        FileMissingError() {
            super("Es wurde keine Datei angegeben! Bitte versuchen Sie erneut eine Datei hochzuladen.");
        }
    }


    static class NoFileOptionSelectedError extends RuntimeException {
        NoFileOptionSelectedError() {
            super("Es wurde keine Datei angegeben! Bitte versuchen Sie erneut eine Datei hochzuladen oder bestätigen Sie," +
                    " dass Ihnen noch keine Datei mit bestehenden Korrekturen vorliegt.");
        }
    }

    static class ProcessYetRunningError extends RuntimeException {
        ProcessYetRunningError() {
            super("Es läuft bereits ein Datenbereiningungsprozess! Bitte versuchen Sie es in ein paar Minuten noch einmal.");
        }
    }
}
