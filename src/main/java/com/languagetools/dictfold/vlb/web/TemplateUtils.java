package com.languagetools.dictfold.vlb.web;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.format.datetime.joda.DateTimeFormatterFactory;

import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class TemplateUtils {

    private enum FileStatus{
        UNKNOWN, VALID, INVALID
    }

    public static String dateFromSeconds(long unixSeconds){
        return new DateTime(unixSeconds).toLocalDateTime().toString("dd.MM.yy HH:mm");
    }


    public static String displayFileSize(long fileSize) {
        return FileUtils.byteCountToDisplaySize(fileSize);
    }

    public static String getFileStatus(File file){
        if (file == null) {
            return FileStatus.UNKNOWN.toString();
        }
        return file.exists() && file.canRead() && file.length() > 0 ? FileStatus.VALID.toString() : FileStatus.INVALID.toString();
    }


}
