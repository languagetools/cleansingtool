package com.languagetools.dictfold.vlb.web;


import com.languagetools.dictfold.vlb.IndexSynonym;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

public abstract class AbstractVlbSynonymService extends AbstractVlbService {

    protected void applySynonyms(String outputFileName, List<IndexSynonym> synonyms) throws IOException {
        String outputFile = inOutputDirectory(outputFileName);
        writeSynonyms(synonyms, outputFile);

        Path sourcePath = Paths.get(outputFile);
        Files.copy(sourcePath, Paths.get(solrCoreDirectory).resolve(sourcePath.getFileName()), StandardCopyOption.REPLACE_EXISTING);
    }


    protected void writeSynonyms(List<IndexSynonym> synonyms, String outputFile) throws IOException {
        FileWriter writer = new FileWriter(outputFile);
        for (IndexSynonym synonym : synonyms){
            writer.write(synonym.getSourceTerm() + "," + synonym.getTargetTerm());
            writer.write("\n");
        }
        writer.flush();
    }


}
