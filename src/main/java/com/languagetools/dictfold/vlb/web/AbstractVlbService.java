package com.languagetools.dictfold.vlb.web;


import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AbstractVlbService {

    @Value("${solr.core_directory}")
    protected String solrCoreDirectory;

    @Value("${output_directory}")
    protected String outputDirectory;


    protected String inOutputDirectory(String fileName) {
        return new File(outputDirectory, fileName).getAbsolutePath();
    }


    protected AtomicInteger runStatus = new AtomicInteger();


    public boolean isProcessRunning() {
        return runStatus.get() != 0;
    }

}
