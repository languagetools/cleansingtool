package com.languagetools.dictfold.vlb.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@RequestMapping("/synonyms/v1")
public class SynonymController extends AbstractController {


    private static Logger LOGGER = Logger.getLogger(SynonymController.class.getName());

    private static final String UPLOADED_SYNONYMS_EXCEL_FILENAME_XLSX = "uploaded_synonyms.xlsx";
    private static final String UPLOADED_SYNONYMS_EXCEL_FILENAME_XLS = "uploaded_synonyms.xls";

    @Autowired
    private SynonymService synonymService;

    @RequestMapping(value = "upload_synonyms")
    public String uploadSynonyms() {
        return "synonyms_upload";
    }

    @RequestMapping(value = "/")
    public String showSynonyms() {
        return "synonyms";
    }

    @RequestMapping(value = "synonyms_uploaded", method= RequestMethod.POST)
    public String synonymsUploaded(@RequestParam("file") MultipartFile correctionMultiFileToUpload, HttpSession session) {

        throwIfAnotherProcessIsRunningYet();

        if (!correctionMultiFileToUpload.isEmpty()) {
            try {
                String localSynoymOutputName = correctionMultiFileToUpload.getOriginalFilename().endsWith(".xlsx")  //TODO instead of this and code in syn excelformat => try catch logic there to try both formats
                        ? UPLOADED_SYNONYMS_EXCEL_FILENAME_XLSX : UPLOADED_SYNONYMS_EXCEL_FILENAME_XLS;

                File uploadedSynonymFile = upload(correctionMultiFileToUpload, localSynoymOutputName);
                backupFileUploadedFile(uploadedSynonymFile);
                Future<Boolean> success = synonymService.applySynonymsToIndex(uploadedSynonymFile);
                session.setAttribute("success", success);
                return "synonyms_applied";
            } catch (Exception exx) {
                LOGGER.log(Level.SEVERE, "Error while uploading synonym file", exx);
                throw new UploadError();
            }
        } else {
            throw new FileMissingError();
        }
    }


    private void throwIfAnotherProcessIsRunningYet() {
        if (synonymService.isProcessRunning()) {
            throw new ProcessYetRunningError();
        }

    }


}
