package com.languagetools.dictfold.vlb.web;

import com.languagetools.dictfold.vlb.VlbCorrectionType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController extends AbstractController {

    @RequestMapping("/")
    String showHomepage(Model model) {
        addCorrectionTypeToModel(VlbCorrectionType.AUTHORS, model);
        return "cleansing";
    }
}
