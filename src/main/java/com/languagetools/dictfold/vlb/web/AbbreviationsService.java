package com.languagetools.dictfold.vlb.web;


import com.languagetools.dictfold.vlb.AbbreviationExcelInputFormat;
import com.languagetools.dictfold.vlb.IndexSynonym;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class AbbreviationsService extends AbstractVlbSynonymService {


    private final static Logger LOGGER = Logger.getLogger(AbbreviationsService.class.getName());
    private static final String ABBREVIATIONS_FILENAME = "abbreviations.txt";


    @Async
    public Future<Boolean> applyAbbreviationsToIndex(File uploadedAbbreviationFile) throws IOException {
        boolean success = false;
        try {
            runStatus.set(1);
            AbbreviationExcelInputFormat synonymExcelInputFormat = new AbbreviationExcelInputFormat(uploadedAbbreviationFile);
            List<IndexSynonym> synonyms = synonymExcelInputFormat.readAbbreviations();
            applySynonyms(ABBREVIATIONS_FILENAME, synonyms);
            success = true;
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Could not apply abbreviations to index", e);
        } finally {
            runStatus.set(0);
        }

        return new AsyncResult<>(success);
    }
}
