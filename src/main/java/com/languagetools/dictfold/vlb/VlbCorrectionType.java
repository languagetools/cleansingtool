package com.languagetools.dictfold.vlb;


public enum VlbCorrectionType {
    DNB_TAGS("DNB Schlagworte"),
    TAGS("Schlagworte"),
    PUBLISHER("Verlage"),
    AUTHORS("Autoren");

    private final String userString;


    VlbCorrectionType(String userString) {
        this.userString = userString;
    }


    public String getUserString() {
        return userString;
    }
}
