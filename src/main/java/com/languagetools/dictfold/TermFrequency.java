package com.languagetools.dictfold;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TermFrequency { //TODO: add score? gfk rank etc included!?!

    private final static Logger LOGGER = Logger.getLogger(TermFrequency.class.getName());

    private String term;
    private int frequency;


    private TermFrequency(String term, int frequency) {
        this.term = term;
        this.frequency = frequency;
    }


    public static TermFrequency fromInputLine(String line, String separator) {
        String[] lineparts = line.trim().split(separator);
        Preconditions.checkArgument(lineparts.length > 1);
        TermFrequency termFrequency = TermFrequency.of(lineparts[1], Integer.parseInt(lineparts[0]));
        return termFrequency;
    }


    public static TermFrequency of(String term, int frequency) {
        return new TermFrequency(term, frequency);
    }


    public String getTerm() {
        return term;
    }

    public int getFrequency() {
        return frequency;
    }


    public static List<TermFrequency> getTermFrequenciesFromFile(File freqList, String lineSeparator) {
        List<TermFrequency> termFrequencies = Lists.newArrayList();

        try (
                Scanner scanner = new Scanner(freqList, "UTF-8");
        )
        {
            while(scanner.hasNext()) {
                String line = scanner.nextLine().trim();
                if (line.isEmpty()) {
                    continue;
                }

                try {
                    termFrequencies.add(TermFrequency.fromInputLine(line, lineSeparator));
                } catch (Exception exx){
                    LOGGER.log(Level.WARNING, "Could not process line:" + line, exx);
                    continue;
                }

            }
        } catch (IOException e) {
            throw new IllegalArgumentException("Could not read term frequencies from input file. ", e);
        }
        return termFrequencies;
    }



    public static class Ordering extends com.google.common.collect.Ordering<TermFrequency> {

        @Override
        public int compare(TermFrequency left, TermFrequency right) {
            return Ints.compare(left.frequency, right.frequency);
        }
    }


    public static class FrequencySetComparator extends Ordering {

        @Override
        public int compare(TermFrequency a, TermFrequency b) {
            int sourceFrequencyCompared = super.compare(b, a);
            if (sourceFrequencyCompared != 0) {
                return sourceFrequencyCompared;
            }
            return a.getTerm().compareTo(b.getTerm());
        }
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }


    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }


    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
}
