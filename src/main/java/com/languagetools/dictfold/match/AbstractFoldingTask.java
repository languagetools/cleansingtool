package com.languagetools.dictfold.match;

import com.languagetools.dictfold.CorrectionCandidate;
import com.languagetools.dictfold.TermFrequency;

import java.util.List;
import java.util.concurrent.Callable;

public abstract class AbstractFoldingTask implements Callable<CorrectionCandidate> {

    protected AbstractFolder folder;
    protected final TermFrequency termFrequency;


    public AbstractFoldingTask(AbstractFolder folder, TermFrequency termFrequency) {
        this.folder = folder;
        this.termFrequency = termFrequency;
    }

    @Override
    public CorrectionCandidate call() throws Exception {

        List<TermFrequency> matches = folder.getMatches(termFrequency.getTerm());
        CorrectionCandidate correctionCandidate = createCorrectionCandidate(matches);

        return correctionCandidate;
    }

    protected abstract CorrectionCandidate createCorrectionCandidate(List<TermFrequency> matches);


    public TermFrequency getTermFrequency() {
        return termFrequency;
    }
}
