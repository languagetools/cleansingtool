package com.languagetools.dictfold.match.fuzzy;

import com.languagetools.dictfold.TermFrequency;

public class NameLevenshteinFoldingTask extends AbstractLevenshteinFoldingTask {

    public NameLevenshteinFoldingTask(NameLevenshteinFolder levenshteinFolder, TermFrequency termFrequency) {
        super(levenshteinFolder, termFrequency);
    }
}
