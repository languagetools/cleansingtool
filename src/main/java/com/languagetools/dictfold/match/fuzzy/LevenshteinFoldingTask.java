package com.languagetools.dictfold.match.fuzzy;

import com.languagetools.dictfold.TermFrequency;

public class LevenshteinFoldingTask extends AbstractLevenshteinFoldingTask {

    public LevenshteinFoldingTask(LevenshteinFolder levenshteinFolder, TermFrequency termFrequency) {
        super(levenshteinFolder, termFrequency);
    }
}
