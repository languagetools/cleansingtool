package com.languagetools.dictfold.match.fuzzy;

import com.languagetools.dictfold.Indexer;
import com.languagetools.dictfold.TermFrequency;
import com.languagetools.dictfold.configuration.LevenshteinConfiguration;
import com.languagetools.dictfold.match.AbstractFoldingTask;
import com.languagetools.dictfold.match.name.NameMatchUtils;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.FuzzyQuery;

import java.io.File;
import java.util.List;

public class LevenshteinFolder extends AbstractLevenshteinFolder {


    public LevenshteinFolder(File indexDirectory, LevenshteinConfiguration levenshteinConfiguration) {
        super("LS", indexDirectory, levenshteinConfiguration);
    }


    @Override
    public List<TermFrequency> getMatches(String term) {
        FuzzyQuery query = new FuzzyQuery(new Term(Indexer.TERM_FIELD_NORMALIZED, Indexer.normalizeErasingSpecialChars(term)),
                levenshteinConfiguration.getMaximalEditDistance(), levenshteinConfiguration.getPrefixLength());
        return search(query);
    }


    @Override
    public AbstractFoldingTask createFoldingTask(TermFrequency termFrequency) {
        return new LevenshteinFoldingTask(this, termFrequency);
    }


    @Override
    public boolean shallStartFoldingTaskFor(TermFrequency termFrequency) {
        return super.shallStartFoldingTaskFor(termFrequency)
                && fullNameShallNotBeSkipped(termFrequency.getTerm());
    }


    private boolean fullNameShallNotBeSkipped(String term) {
        return levenshteinConfiguration.shallApplyToFullNameCandidates() || !NameMatchUtils.isFullNameCandidate(term);
    }
}
