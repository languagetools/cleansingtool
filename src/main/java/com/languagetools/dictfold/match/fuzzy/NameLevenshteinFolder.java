package com.languagetools.dictfold.match.fuzzy;


import com.languagetools.dictfold.Indexer;
import com.languagetools.dictfold.TermFrequency;
import com.languagetools.dictfold.configuration.LevenshteinConfiguration;
import com.languagetools.dictfold.match.AbstractFoldingTask;
import com.languagetools.dictfold.match.name.NameMatchUtils;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;

import java.io.File;
import java.util.List;

public class NameLevenshteinFolder extends AbstractLevenshteinFolder {


    public NameLevenshteinFolder(File indexDirectory, LevenshteinConfiguration levenshteinConfiguration) {
        super("NLS", indexDirectory, levenshteinConfiguration);
    }


    @Override
    public List<TermFrequency> getMatches(String term) {

        String lastName = NameMatchUtils.getLastName(term);
        lastName = Indexer.normalizeErasingSpecialChars(lastName);

        Query lastNameFuzzyQuery = new FuzzyQuery(new Term(Indexer.LAST_NAME_FIELD_NORMALIZED, lastName),
                levenshteinConfiguration.getMaximalEditDistance(), levenshteinConfiguration.getPrefixLength());

        String firstName = NameMatchUtils.getFirstName(term);
        firstName = Indexer.normalizeErasingSpecialChars(firstName);

        Query firstNameFuzzyQuery = new FuzzyQuery(new Term(Indexer.FIRST_NAME_FIELD_NORMALIZED, firstName),
                levenshteinConfiguration.getMaximalEditDistance(), 1);

        BooleanQuery booleanQuery = new BooleanQuery();

        BooleanQuery firstNameFuzzyLastNameExactQuery = new BooleanQuery();
        firstNameFuzzyLastNameExactQuery.add(firstNameFuzzyQuery, BooleanClause.Occur.MUST);
        firstNameFuzzyLastNameExactQuery.add(new TermQuery(new Term(Indexer.LAST_NAME_FIELD_NORMALIZED, lastName)), BooleanClause.Occur.MUST);

        BooleanQuery lastNameFuzzyFirstNameExactQuery = new BooleanQuery();
        lastNameFuzzyFirstNameExactQuery.add(lastNameFuzzyQuery, BooleanClause.Occur.MUST);
        lastNameFuzzyFirstNameExactQuery.add(new TermQuery(new Term(Indexer.FIRST_NAME_FIELD_NORMALIZED, firstName)), BooleanClause.Occur.MUST);


        booleanQuery.add(firstNameFuzzyLastNameExactQuery, BooleanClause.Occur.SHOULD);
        booleanQuery.add(lastNameFuzzyFirstNameExactQuery, BooleanClause.Occur.SHOULD);

        return search(booleanQuery);
    }


    @Override
    public AbstractFoldingTask createFoldingTask(TermFrequency termFrequency) {
        return new NameLevenshteinFoldingTask(this, termFrequency);
    }


    @Override
    public boolean shallStartFoldingTaskFor(TermFrequency termFrequency) {
        return super.shallStartFoldingTaskFor(termFrequency)
                && NameMatchUtils.isFullNameCandidate(termFrequency.getTerm());
    }
}
