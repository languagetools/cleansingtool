package com.languagetools.dictfold.match.fuzzy;

import com.languagetools.dictfold.CorrectionCandidate;
import com.languagetools.dictfold.TermFrequency;
import com.languagetools.dictfold.match.AbstractFolder;
import com.languagetools.dictfold.match.AbstractFoldingTask;

import java.util.List;

//TODO: test
public abstract class AbstractLevenshteinFoldingTask extends AbstractFoldingTask {


    public AbstractLevenshteinFoldingTask(AbstractFolder folder, TermFrequency termFrequency) {
        super(folder, termFrequency);
    }


    @Override
    protected CorrectionCandidate createCorrectionCandidate(List<TermFrequency> matches) {
        if (!matches.isEmpty()) {
            TermFrequency bestMatch = new TermFrequency.Ordering().max(matches);
            if (bestMatch.getFrequency() > termFrequency.getFrequency() * 10) {
                return CorrectionCandidate.of(termFrequency, bestMatch, folder.getIdentifier());
            }
        }
        return CorrectionCandidate.of(termFrequency, null, folder.getIdentifier());
    }
}
