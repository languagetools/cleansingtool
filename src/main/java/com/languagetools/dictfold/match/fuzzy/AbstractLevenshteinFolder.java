package com.languagetools.dictfold.match.fuzzy;


import com.languagetools.dictfold.TermFrequency;
import com.languagetools.dictfold.configuration.LevenshteinConfiguration;
import com.languagetools.dictfold.match.AbstractFolder;
import org.apache.lucene.search.Query;

import java.io.File;
import java.util.List;

public abstract class AbstractLevenshteinFolder extends AbstractFolder {


    private static final int MAX_CANDIDATES = 50;
    protected LevenshteinConfiguration levenshteinConfiguration;


    public AbstractLevenshteinFolder(String identifier, File indexDirectory, LevenshteinConfiguration levenshteinConfiguration) {
        super(identifier, indexDirectory);
        this.levenshteinConfiguration = levenshteinConfiguration;
    }



    protected List<TermFrequency> search(Query query) {
        return super.search(query, MAX_CANDIDATES);
    }


    @Override
    public boolean shallStartFoldingTaskFor(TermFrequency termFrequency) {
        return termFrequency.getTerm().length() >= levenshteinConfiguration.getMinimalTermLength()
                && termFrequency.getFrequency() <= levenshteinConfiguration.getFrequencyThreshold();
    }
}
