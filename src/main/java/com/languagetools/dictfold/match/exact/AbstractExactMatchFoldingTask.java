package com.languagetools.dictfold.match.exact;


import com.languagetools.dictfold.CorrectionCandidate;
import com.languagetools.dictfold.TermFrequency;
import com.languagetools.dictfold.match.AbstractFolder;
import com.languagetools.dictfold.match.AbstractFoldingTask;

import java.util.List;

public class AbstractExactMatchFoldingTask extends AbstractFoldingTask {

    public AbstractExactMatchFoldingTask(AbstractFolder folder, TermFrequency termFrequency) {
        super(folder, termFrequency);
    }


    //TODO test
    @Override
    protected CorrectionCandidate createCorrectionCandidate(List<TermFrequency> matches) {
        if (!matches.isEmpty()) {
            TermFrequency bestMatch = new TermFrequency.Ordering().max(matches);
            if (bestMatch.getFrequency() >= termFrequency.getFrequency() && !bestMatch.getTerm().equals(termFrequency.getTerm())){
                return CorrectionCandidate.of(termFrequency, bestMatch, folder.getIdentifier());
            }
        }
        return CorrectionCandidate.of(termFrequency, null, folder.getIdentifier());
    }
}
