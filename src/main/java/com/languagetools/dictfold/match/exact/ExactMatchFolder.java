package com.languagetools.dictfold.match.exact;


import com.languagetools.dictfold.Indexer;
import com.languagetools.dictfold.TermFrequency;
import com.languagetools.dictfold.match.AbstractFolder;
import com.languagetools.dictfold.match.AbstractFoldingTask;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;


import java.io.File;
import java.io.IOException;
import java.util.List;

public class ExactMatchFolder extends AbstractFolder {

    private static final int MAX_CANDIDATES = 50;

    @Override
    public AbstractFoldingTask createFoldingTask(TermFrequency termFrequency) {
        return new ExactMatchFoldingTask(this, termFrequency);
    }


    @Override
    public boolean shallStartFoldingTaskFor(TermFrequency termFrequency) {
        return true;
    }


    public ExactMatchFolder(File indexDirectory) {
        super("EX", indexDirectory);
    }


    @Override
    public List<TermFrequency> getMatches(String term) throws IOException {
        term = Indexer.normalizeErasingSpecialChars(term);
        Query query = new TermQuery(new Term(Indexer.TERM_FIELD_NORMALIZED, term));

        return search(query, MAX_CANDIDATES);
    }

}
