package com.languagetools.dictfold.match.exact;


import com.languagetools.dictfold.TermFrequency;

public class ExtendedExactMatchFoldingTask extends AbstractExactMatchFoldingTask {

    public ExtendedExactMatchFoldingTask(ExtendedExactMatchFolder exactMatchFolder, TermFrequency termFrequency) {
        super(exactMatchFolder, termFrequency);
    }
}
