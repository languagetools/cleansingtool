package com.languagetools.dictfold.match.name;

import com.languagetools.dictfold.match.AbstractFoldingTask;
import com.languagetools.dictfold.CorrectionCandidate;
import com.languagetools.dictfold.TermFrequency;

import java.util.List;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class NameVariantMatchFoldingTask extends AbstractFoldingTask {

    public NameVariantMatchFoldingTask(NameVariantMatchFolder nameMatchFolder, TermFrequency termFrequency) {
        super(nameMatchFolder, termFrequency);
    }

    @Override
    protected CorrectionCandidate createCorrectionCandidate(List<TermFrequency> matches) {
        if (matches!= null && !matches.isEmpty()) {
            List<TermFrequency> bestMatches = new TermFrequency.Ordering().greatestOf(matches, 5);
            for (TermFrequency bestMatch : bestMatches) {
                if (bestMatch.getFrequency() > termFrequency.getFrequency() && firstNameMatchIsValid(bestMatch)){
                    return CorrectionCandidate.of(termFrequency, bestMatch, folder.getIdentifier());
                }
            }
        }
        return CorrectionCandidate.of(termFrequency, null, folder.getIdentifier());
    }


    private boolean firstNameMatchIsValid(TermFrequency potentialMatch) {
        String firstNameOfOriginalTerm = NameMatchUtils.getFirstName(termFrequency.getTerm());
        String firstNameOfPotentialCorrectionTerm = NameMatchUtils.getFirstName(potentialMatch.getTerm());

        if(NameMatchUtils.numberOfFirstNames(firstNameOfOriginalTerm) == 1){
            return isAnAcronym(firstNameOfOriginalTerm);
        }

        return prefixesOfFirstNamePartsDoMatch(firstNameOfOriginalTerm, firstNameOfPotentialCorrectionTerm);
    }


    private boolean prefixesOfFirstNamePartsDoMatch(String firstNameOfOriginalTerm, String firstNameOfPotentialCorrectionTerm) {
        String[] firstNamePartsOfOriginalTerm = NameMatchUtils.splitFirstName(firstNameOfOriginalTerm);
        String[] firstNamePartsOfCorrectionTerm = NameMatchUtils.splitFirstName(firstNameOfPotentialCorrectionTerm);
        for (int i = 0; i < max(firstNamePartsOfCorrectionTerm.length, firstNamePartsOfOriginalTerm.length); i++) {
            String correctionPart = firstNamePartsOfCorrectionTerm[i];
            String originalPart = firstNamePartsOfOriginalTerm[i];
            int prefixLength = min(3, min(correctionPart.length(), originalPart.length()));
            if (!correctionPart.substring(0, prefixLength).equalsIgnoreCase(originalPart.substring(0, prefixLength))){
                return false;
            }
        }
        return true;
    }

    private boolean isAnAcronym(String firstNameOfOriginalTerm) {
        return NameMatchUtils.hasShortenedParts(firstNameOfOriginalTerm);

    }


}
