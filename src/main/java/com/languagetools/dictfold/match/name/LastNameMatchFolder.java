package com.languagetools.dictfold.match.name;

import com.languagetools.dictfold.match.AbstractFolder;
import com.languagetools.dictfold.match.AbstractFoldingTask;
import com.languagetools.dictfold.Indexer;
import com.languagetools.dictfold.TermFrequency;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;

import java.io.File;
import java.io.IOException;
import java.util.List;


public class LastNameMatchFolder extends AbstractFolder {

    public static final int MAX_CANDIDATES = 50;

    public LastNameMatchFolder(File indexDirectory) {
        super("LN", indexDirectory);
    }


    @Override
    public AbstractFoldingTask createFoldingTask(TermFrequency termFrequency) {
        return new LastNameMatchFoldingTask(this, termFrequency);
    }


    @Override
    public boolean shallStartFoldingTaskFor(TermFrequency termFrequency) {
        return NameMatchUtils.isLastNameCandidate(termFrequency.getTerm());
    }


    @Override
    public List<TermFrequency> getMatches(String term) throws IOException {

        String lastName = NameMatchUtils.getLastName(term);
        lastName = Indexer.normalizeErasingSpecialChars(lastName);
        Query query = new TermQuery(new Term(Indexer.LAST_NAME_FIELD_NORMALIZED, lastName));
        try {
            TopDocs docs = searcher.search(query, MAX_CANDIDATES);
            List<TermFrequency> matches = collectMatches(docs);
            return matches;
        } catch (IOException e) {
            throw new IllegalStateException("Search on index failed. ", e);
        }
    }
}
