package com.languagetools.dictfold.match.name;


import com.languagetools.dictfold.match.AbstractFoldingTask;
import com.languagetools.dictfold.CorrectionCandidate;
import com.languagetools.dictfold.TermFrequency;

import java.util.List;

public class LastNameMatchFoldingTask extends AbstractFoldingTask {


    public LastNameMatchFoldingTask(LastNameMatchFolder nameMatchFolder, TermFrequency termFrequency) {
        super(nameMatchFolder, termFrequency);
    }

    @Override
    protected CorrectionCandidate createCorrectionCandidate(List<TermFrequency> matches) {
        if (!matches.isEmpty()) {  //TODO: test
            List<TermFrequency> bestMatches = new TermFrequency.Ordering().greatestOf(matches, 2);
            TermFrequency bestMatch = bestMatches.get(0);
            if (firstMatchIsBestMatchByFar(bestMatches) && bestMatch.getFrequency() > termFrequency.getFrequency()){
                return CorrectionCandidate.of(termFrequency, bestMatch, folder.getIdentifier());
            }
        }
        return CorrectionCandidate.of(termFrequency, null, folder.getIdentifier());
    }


    private boolean firstMatchIsBestMatchByFar(List<TermFrequency> bestMatches) {
        TermFrequency bestMatch = bestMatches.get(0);
        return bestMatches.size() < 2 || bestMatch.getFrequency() > bestMatches.get(1).getFrequency() * 2;
    }
}
