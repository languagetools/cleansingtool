package com.languagetools.dictfold.match.name;

import com.languagetools.dictfold.match.AbstractFolder;
import com.languagetools.dictfold.match.AbstractFoldingTask;
import com.languagetools.dictfold.Indexer;
import com.languagetools.dictfold.TermFrequency;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;

import java.io.File;
import java.io.IOException;
import java.util.List;


public class NameVariantMatchFolder extends AbstractFolder {

    public static final int MAX_CANDIDATES = 50;

    public NameVariantMatchFolder(File indexDirectory) {
        super("FN", indexDirectory);
    }


    @Override
    public AbstractFoldingTask createFoldingTask(TermFrequency termFrequency) {
        return new NameVariantMatchFoldingTask(this, termFrequency);
    }


    @Override
    public boolean shallStartFoldingTaskFor(TermFrequency termFrequency) {
        return NameMatchUtils.isFullNameCandidate(termFrequency.getTerm());
    }


    @Override
    public List<TermFrequency> getMatches(String term) throws IOException {

        String lastName = NameMatchUtils.getLastName(term);
        lastName = Indexer.normalizeErasingSpecialChars(lastName);
        Query lastNameQuery = new TermQuery(new Term(Indexer.LAST_NAME_FIELD_NORMALIZED, lastName));
        String firstName = NameMatchUtils.getFirstName(term);
        firstName = NameMatchUtils.abbreviateFirstName(firstName);
        Query firstNameQuery = new TermQuery(new Term(Indexer.FIRST_NAME_FIELD_ABBREVIATED, firstName));

        BooleanQuery booleanQuery = new BooleanQuery();
        booleanQuery.add(lastNameQuery, BooleanClause.Occur.MUST);
        booleanQuery.add(firstNameQuery, BooleanClause.Occur.MUST);

        try {
            TopDocs docs = searcher.search(booleanQuery, MAX_CANDIDATES);
            List<TermFrequency> matches = collectMatches(docs);
            return matches;
        } catch (IOException e) {
            throw new IllegalStateException("Search on index failed. ", e);
        }
    }}
