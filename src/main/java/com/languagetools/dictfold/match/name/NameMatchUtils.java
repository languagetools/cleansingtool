package com.languagetools.dictfold.match.name;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import com.languagetools.dictfold.Indexer;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.regex.Pattern;

//TODO: provide different Name formats (order, separator?)
public class NameMatchUtils {

    public static String getLastName(String wholeName) {
        String[] nameParts = splitIntoNameParts(wholeName);
        return StringUtils.trimToEmpty(nameParts[0]);
    }


    public static String getFirstName(String wholeName) {
        String[] nameParts = splitIntoNameParts(wholeName);
        Preconditions.checkArgument(nameParts.length > 1);
        return StringUtils.trimToEmpty(nameParts[1]);
    }


    private static String[] splitIntoNameParts(String wholeName) {
        Preconditions.checkArgument(StringUtils.isNotBlank(wholeName));
        return StringUtils.split(wholeName, ",");
    }


    public static boolean isFullNameCandidate(String input) {
        //should match Ji, L.
        return splitIntoNameParts(input).length == 2
                && Indexer.normalizeErasingSpecialChars(getLastName(input)).length() > 1
                && Indexer.normalizeErasingSpecialChars(getFirstName(input)).length() > 0;
    }


    public static boolean isLastNameCandidate(String input) {
        return Pattern.matches("^[^\\s]+$", Indexer.normalizeErasingSpecialChars(input));
    }


    public static String abbreviateFirstName(String firstName) {
        String[] nameParts = splitFirstName(firstName);
        String[] normalizedParts = new String[nameParts.length];
        for (int i=0; i < nameParts.length; i++) {
            normalizedParts[i] = nameParts[i].substring(0, 1);
        }
        return Joiner.on(" ").skipNulls().join(normalizedParts);
    }


    public static String[] splitFirstName(String firstName) {
        final String normalizedFirstName = Indexer.normalize(firstName);
        String[] firstNameParts = normalizedFirstName.split("[\\s\\.\\-]");
        List<String> filteredFirstNameParts= FluentIterable.of(firstNameParts).filter(new Predicate<String>() {
            @Override
            public boolean apply(String part) {
                return (part != null && part.length() > 0);
            }
        }).transform(new Function<String, String>() {
            @Override
            public String apply(String input) {
                String normalizedPart = StringUtils.strip(input, " .-");
                normalizedPart = StringUtils.trimToEmpty(normalizedPart);
                return normalizedPart;
            }
        }).toList();
        return Iterables.toArray(filteredFirstNameParts, String.class);
    }


    public static int numberOfFirstNames(String firstName) {
        return splitFirstName(firstName).length;
    }

    public static boolean hasShortenedParts(String firstName){
        String[] firstNameParts = splitFirstName(firstName);
        for (int i = 0; i < firstNameParts.length ;i ++){
            if(firstNameParts[i].length() < 3) {
                return true;
            }
        }
        return false;
    }

}
