package com.languagetools.dictfold.match;

import com.google.common.collect.Lists;
import com.languagetools.dictfold.Indexer;
import com.languagetools.dictfold.TermFrequency;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.apache.lucene.store.MMapDirectory;

import java.io.File;
import java.io.IOException;
import java.util.List;


public abstract class AbstractFolder {

    private DirectoryReader reader;
    protected IndexSearcher searcher;
    private final String identifier;
    private final File indexDirectory;


    public abstract AbstractFoldingTask createFoldingTask(TermFrequency termFrequency);
    public abstract boolean shallStartFoldingTaskFor(TermFrequency termFrequency);


    public AbstractFolder(String identifier, File indexDirectory) {
        this.identifier = identifier;
        this.indexDirectory = indexDirectory;
    }


    public void initializeSearcher() {
        try {
            reader = DirectoryReader.open(new MMapDirectory(indexDirectory));
        } catch (IOException e) {
            throw new IllegalArgumentException("Could not open index directory. ", e);
        }
        searcher = new IndexSearcher(reader);
    }


    public int getNumDocs() {
        return reader.numDocs();
    }



    protected List<TermFrequency> search(Query query, int maxCandidates) {
        try {
            TopDocs docs = searcher.search(query, maxCandidates);
            List<TermFrequency> matches = collectMatches(docs);
            return matches;
        } catch (IOException e) {
            throw new IllegalStateException("Search on index failed. ", e);
        }
    }



    protected List<TermFrequency> collectMatches(TopDocs docs) throws IOException {
        List<TermFrequency> matches = Lists.newArrayList();
        for(ScoreDoc doc: docs.scoreDocs) {
            Document document = searcher.doc(doc.doc);
            String term = document.getField(Indexer.TERM_FIELD).stringValue();
            int frequency = document.getField(Indexer.FREQUENCY_FIELD).numericValue().intValue();
            TermFrequency match = TermFrequency.of(term, frequency);

            matches.add(match);
        }
        return matches;
    }

    public int getTermFrequency(String term) {
        Query query = new TermQuery(new Term(Indexer.TERM_FIELD, term));
        try {
            TopDocs docs = searcher.search(query, 1);
            if (docs.scoreDocs.length > 0) {
                return searcher.doc(docs.scoreDocs[0].doc).getField(Indexer.FREQUENCY_FIELD).numericValue().intValue();
            } else {
                return -1;
            }
        } catch (IOException e) {
            throw new IllegalStateException("Search on index failed. ", e);
        }
    }


    public abstract List<TermFrequency> getMatches(String input) throws IOException;


    public String getIdentifier() {
        return identifier;
    }


    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, "reader", "searcher", "indexDirectory");
    }


    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, Lists.newArrayList("reader", "searcher", "indexDirectory"));
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
