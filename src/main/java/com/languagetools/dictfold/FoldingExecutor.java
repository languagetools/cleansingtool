package com.languagetools.dictfold;


import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.languagetools.dictfold.match.AbstractFolder;
import com.languagetools.dictfold.vlb.VlbCorrectionTool;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.Collections.synchronizedList;

public class FoldingExecutor {
//TODO: remove reporter
    private final static Logger LOGGER = Logger.getLogger(FoldingExecutor.class.getName());

    private static final int POOL_SIZE = 10;
    private final Set<AbstractFolder> folders = Sets.newHashSet();
    private final String separator;


    public FoldingExecutor(String separator, Collection<AbstractFolder> folders) throws FileNotFoundException {
        this.separator = separator;
        this.folders.addAll(folders);
    }


    public List<CorrectionCandidate> foldDictionaryInParallel(Collection<TermFrequency> termFrequencies) {
        ExecutorService executor = Executors.newFixedThreadPool(POOL_SIZE);
        List<Future<CorrectionCandidate>> tasks = new ArrayList<>();

        for(AbstractFolder folder: folders) {
            folder.initializeSearcher();
        }

        List<CorrectionCandidate> results =  synchronizedList(Lists.<CorrectionCandidate>newArrayList());

        try {

            Reporter reporter = new Reporter("processed", true, 1000);

            for (TermFrequency termFrequency : termFrequencies) {


                for(AbstractFolder folder : folders) {
                    if (folder.shallStartFoldingTaskFor(termFrequency)) {
                        tasks.add(executor.submit(folder.createFoldingTask(termFrequency)));
                    }
                }

                if (tasks.size() >= POOL_SIZE) {
                    consumeTasks(tasks, results);
                }
                reporter.processed();
            }
            executor.shutdown();
            executor.awaitTermination(10, TimeUnit.MINUTES);
            consumeTasks(tasks,results);
            reporter.done();
        } catch (InterruptedException | ExecutionException exx) {
            throw new IllegalStateException("Error processing frequency list. ", exx);
        }
        return results;
    }


    private void consumeTasks(List<Future<CorrectionCandidate>> tasks, List<CorrectionCandidate> results) throws InterruptedException, ExecutionException {
        for (Future<CorrectionCandidate> task : tasks) {
            CorrectionCandidate result = task.get();
            if (result.getTargetTerm() != null) {
                results.add(result);
            }
        }
        tasks.clear();
    }


    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }


    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
