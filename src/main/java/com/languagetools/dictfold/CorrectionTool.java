package com.languagetools.dictfold;


import com.languagetools.dictfold.configuration.FoldingExecutorFactory;
import com.languagetools.dictfold.configuration.IndexerFactory;
import org.apache.commons.configuration.Configuration;

import java.io.File;
import java.util.Collection;
import java.util.List;

public abstract class CorrectionTool {

    protected Configuration configuration;
    protected FoldingExecutor foldingExecutor;
    protected CorrectionCandidateAggregator candidateAggregator;


    protected CorrectionTool(Configuration configuration) throws Exception {
        this.configuration = configuration;
        foldingExecutor = new FoldingExecutorFactory(configuration).createFoldingExecutor();
        candidateAggregator = new CorrectionCandidateAggregator();
    }


    public List<CorrectionCandidate> createCorrectionCandidates(Collection<TermFrequency> termFrequencies) {
        Indexer index = new IndexerFactory(configuration).createIndexer();
        index.createIndexFromFreqList(termFrequencies);
        List<CorrectionCandidate> correctionCandidates = foldingExecutor.foldDictionaryInParallel(termFrequencies);
        return candidateAggregator.aggregateCorrectionCandidatesByUsedFolder(correctionCandidates);
    }
}
