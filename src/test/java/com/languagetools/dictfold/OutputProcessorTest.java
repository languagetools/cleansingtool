package com.languagetools.dictfold;

import com.google.common.collect.Lists;
import com.languagetools.dictfold.vlb.OutputProcessor;
import org.junit.Test;

import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.*;


public class OutputProcessorTest {

    private OutputProcessor outputProcessor;
    private CorrectionCandidateTestFormat correctionCandidateTestFormat;


    private void givenOutputProcessor() {
        correctionCandidateTestFormat = new CorrectionCandidateTestFormat(Collections.<CorrectionCandidate>emptyList());
        outputProcessor = new OutputProcessor(correctionCandidateTestFormat);
    }


    @Test(expected = NullPointerException.class)
    public void nullWriterThrows() {
        new OutputProcessor(null);
    }


    @Test
    public void emptyResultWritesEmptyOutput() {
        givenOutputProcessor();
        whenNoCandidatesAreProcessed();
        thenTheseCorrectionsAreTheOutput();
    }


    @Test
    public void simpleResultWritesValidOutput() {
        givenOutputProcessor();
        CorrectionCandidate candidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("bar").addFolder("LN").build();
        CorrectionCandidate secondCandidate = new CorrectionCandidateTestBuilder().sourceTerm("baz").targetTerm("bäm").addFolder("LS").build();
        whenCandidatesAreProcessed(candidate, secondCandidate);
        thenTheseCorrectionsAreTheOutput(secondCandidate, candidate);
        andThereAreNoAutomaticCorrections();
    }


    @Test
    public void sortFirstOnSourceTermThenOnItsFrequencyThenOnTheTargetTerm() {
        givenOutputProcessor();
        CorrectionCandidate candidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").sourceFreq(2).targetTerm("zoo").targetFreq(2).addFolder("LN").build();
        CorrectionCandidate secondCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("bäm").addFolder("LS").build();
        CorrectionCandidate thirdCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").sourceFreq(2).targetTerm("aoo").targetFreq(2).addFolder("FN").build();
        CorrectionCandidate fourthCandidate = new CorrectionCandidateTestBuilder().sourceTerm("fi").targetTerm("fo").targetFreq(2).addFolder("FN").build();
        CorrectionCandidate candidateStartingWithNoLetter = new CorrectionCandidateTestBuilder().sourceTerm(" $zo").targetTerm("fo").targetFreq(2).addFolder("FN").build();
        CorrectionCandidate candidateStartingWithCapitalLetter = new CorrectionCandidateTestBuilder().sourceTerm("Zo").targetTerm("fo").targetFreq(2).addFolder("FN").build();
        whenCandidatesAreProcessed(candidate, secondCandidate, thirdCandidate, fourthCandidate, candidateStartingWithNoLetter,
                                   candidateStartingWithCapitalLetter);
        thenTheseCorrectionsAreTheOutput(candidateStartingWithNoLetter, candidateStartingWithCapitalLetter, fourthCandidate, thirdCandidate, candidate,
                                         secondCandidate);
        andThereAreNoAutomaticCorrections();
    }


    @Test
    public void outputIsNotFoldedIfFrequencyOfCandidatesIsNotEqual() {
        givenOutputProcessor();
        CorrectionCandidate candidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").sourceFreq(2).targetTerm("bar").targetFreq(2).addFolder("LN").build();
        CorrectionCandidate secondCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").sourceFreq(1).targetTerm("bar").targetFreq(2).addFolder("LS").build();
        whenCandidatesAreProcessed(candidate, secondCandidate);
        thenTheseCorrectionsAreTheOutput(candidate, secondCandidate);
        andThereAreNoAutomaticCorrections();
    }


    @Test
    public void correctionsWithExactMatchAreAutomaticCorrections() {
        givenOutputProcessor();
        CorrectionCandidate candidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("bar").addFolder("EX").build();
        CorrectionCandidate secondCandidate = new CorrectionCandidateTestBuilder().sourceTerm("bar").targetTerm("schnu").addFolder("LS").build();
        whenCandidatesAreProcessed(candidate, secondCandidate);
        thenTheseCorrectionsAreTheOutput(secondCandidate);
        andTheseAreTheAutomaticCorrections(new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("bar").addFolder("EX").approved().build());
    }


    @Test
    public void automaticCorrectionIfSameCorrectionWithCanonicalFormAsSourceExists() {
        givenOutputProcessor();

        CorrectionCandidate candidate = new CorrectionCandidateTestBuilder().sourceTerm(" foo")
                        .targetTerm("fii").addFolder("LS").build();

        CorrectionCandidate exactMatchCandidate = new CorrectionCandidateTestBuilder().sourceTerm(" foo")
                .targetTerm("foo").addFolder("EX").build();

        CorrectionCandidate candidateWithCanonicalForm = new CorrectionCandidateTestBuilder().sourceTerm("foo")
                .targetTerm("fii").addFolder("LN").build();

        whenCandidatesAreProcessed(candidate, exactMatchCandidate, candidateWithCanonicalForm);
        thenTheseCorrectionsAreTheOutput(candidateWithCanonicalForm);
        andTheseAreTheAutomaticCorrections(new CorrectionCandidateTestBuilder().sourceTerm(" foo")
                .targetTerm("foo").approved().addFolder("EX").build(),
                new CorrectionCandidateTestBuilder().sourceTerm(" foo")
                        .targetTerm("fii").approved().addFolder("LS").comment("-DUPLIKAT-").build());

    }

    @Test
    public void canonicalDuplicateMatchForAutomaticCorrectionsWorksOnlyForEX() {
        givenOutputProcessor();

        CorrectionCandidate candidate = new CorrectionCandidateTestBuilder().sourceTerm(" foo")
                .targetTerm("fii").addFolder("LS").build();

        CorrectionCandidate nonExactMatchCandidate = new CorrectionCandidateTestBuilder().sourceTerm(" foo")
                .targetTerm("foo").addFolder("LS").build();

        CorrectionCandidate candidateWithCanonicalForm = new CorrectionCandidateTestBuilder().sourceTerm("foo")
                .targetTerm("fii").addFolder("LN").build();

        whenCandidatesAreProcessed(candidate, nonExactMatchCandidate, candidateWithCanonicalForm);
        thenTheseCorrectionsAreTheOutput(candidate, nonExactMatchCandidate, candidateWithCanonicalForm);
        andThereAreNoAutomaticCorrections();
    }



    private void whenCandidatesAreProcessed(CorrectionCandidate... correctionCandidates) {
        outputProcessor.writeCorrectionOutput(Lists.<CorrectionCandidate>newArrayList(correctionCandidates));
    }


    private void whenNoCandidatesAreProcessed() {
        whenCandidatesAreProcessed();
    }


    private void thenTheseCorrectionsAreTheOutput(CorrectionCandidate... expectedCandidates) {
        assertEquals(Lists.newArrayList(expectedCandidates), correctionCandidateTestFormat.getOutputCorrectionCandidates());
    }

    private void andTheseAreTheAutomaticCorrections(CorrectionCandidate... expectedAutomaticCorrections) {
        assertEquals(Lists.newArrayList(expectedAutomaticCorrections), correctionCandidateTestFormat.getAutomaticCorrections());
    }
    private void andThereAreNoAutomaticCorrections() {
        andTheseAreTheAutomaticCorrections();
    }

}