package com.languagetools.dictfold;

import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;


public class CorrectionCandidateAggregatorTest {  //TODO: use test builder



    private CorrectionCandidateAggregator aggregator;


    @Before
    public void initAggregator() {
        aggregator = new CorrectionCandidateAggregator();
    }


    @Test
    public void emptyResultWritesEmptyOutput() {
        assertTrue(aggregator.aggregateCorrectionCandidatesByUsedFolder(Collections.<CorrectionCandidate>emptyList()).isEmpty());
    }


    @Test
    public void simpleResultWritesValidOutput() {

        CorrectionCandidate candidate = CorrectionCandidate.of(TermFrequency.of("foo", 1), TermFrequency.of("bar", 2), "LN");
        CorrectionCandidate secondCandidate = CorrectionCandidate.of(TermFrequency.of("baz", 2), TermFrequency.of("bäm", 1), "LS");
        List<CorrectionCandidate> correctionCandidates = aggregator.aggregateCorrectionCandidatesByUsedFolder(Lists.newArrayList(candidate, secondCandidate));
        assertEquals(2, correctionCandidates.size());
        assertTrue(correctionCandidates.contains(candidate));
        assertTrue(correctionCandidates.contains(secondCandidate));
    }


    @Test
    public void resultWritesFoldedOutput() {
        CorrectionCandidate candidate = CorrectionCandidate.of(TermFrequency.of("foo", 1), TermFrequency.of("bar", 2), "LN");
        CorrectionCandidate secondCandidate = CorrectionCandidate.of(TermFrequency.of("foo", 1), TermFrequency.of("bar", 2), "LS");
        List<CorrectionCandidate> correctionCandidates = aggregator.aggregateCorrectionCandidatesByUsedFolder(Lists.newArrayList(candidate, secondCandidate));
        assertEquals(Lists.newArrayList(CorrectionCandidate.of(TermFrequency.of("foo", 1), TermFrequency.of("bar", 2), "LN", "LS")), correctionCandidates);
    }


    @Test
    public void noFoldingOccursIfFrequencyIsNotEqual() {
        CorrectionCandidate candidate = CorrectionCandidate.of(TermFrequency.of("foo", 2), TermFrequency.of("bar", 2), "LN");
        CorrectionCandidate secondCandidate = CorrectionCandidate.of(TermFrequency.of("foo", 1), TermFrequency.of("bar", 2), "LS");
        List<CorrectionCandidate> correctionCandidates = aggregator.aggregateCorrectionCandidatesByUsedFolder(Lists.newArrayList(candidate, secondCandidate));
        assertEquals(2, correctionCandidates.size());
        assertTrue(correctionCandidates.contains(candidate));
        assertTrue(correctionCandidates.contains(secondCandidate));
    }

}