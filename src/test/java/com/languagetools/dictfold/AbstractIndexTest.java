package com.languagetools.dictfold;

import com.google.common.io.Files;
import com.google.common.io.Resources;
import org.junit.After;
import org.junit.Before;

import java.io.File;
import java.net.URISyntaxException;


public abstract class AbstractIndexTest {
    protected Indexer indexer;
    protected File indexDirectory;


    protected static File getFreqlist()  {
        try {
            return new File(Resources.getResource("test_index.txt").getFile());
        } catch(Exception exx) {
            throw new RuntimeException(exx);
        }
    }


    @Before
    public void setUpIndex() {
        indexDirectory = Files.createTempDir();
        indexer = new Indexer("\t", indexDirectory);
    }


    @After
    public void removeTmpDir() {
        indexDirectory.delete();
    }


    protected int createTestIndex() {
        return indexer.createIndexFromFreqList(TermFrequency.getTermFrequenciesFromFile(getFreqlist(), "\t"));
    }
}
