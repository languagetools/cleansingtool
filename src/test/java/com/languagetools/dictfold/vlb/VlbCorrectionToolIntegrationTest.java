package com.languagetools.dictfold.vlb;


import com.google.common.collect.Lists;
import com.languagetools.dictfold.CorrectionCandidate;
import com.languagetools.dictfold.CorrectionCandidateTestBuilder;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;


//TODO: delete output?
public class VlbCorrectionToolIntegrationTest {

    private VlbCorrectionTool vlbCorrectionTool;
    private VlbFileInput vlbFileInput;

    @Before
    public void init() throws Exception {
        Configuration configuration = new PropertiesConfiguration("config.properties");
        vlbCorrectionTool = new VlbCorrectionTool(configuration);
        vlbFileInput = new VlbFileInput(getResource("../freqList.txt"), "\t");
    }


    @Test
    public void createCorrectionsFromExistingFile() throws Exception {

        File file = vlbCorrectionTool.addNewCorrectionsFromIndex(vlbFileInput, VlbCorrectionType.AUTHORS, "/tmp/dict_fold_integration_test_output.xlsx",
                getResource("correctionCandidates.xlsx"));
        CorrectionCandidateExcelFormat correctionCandidateExcelFormat = new CorrectionCandidateExcelFormat(file, null);

        List<CorrectionCandidate> expectedCorrectionCandidates = Lists.newArrayList(
                candidate().sourceTerm("Benedict, Ralph Paine").sourceFreq(0).targetTerm("Benedict, Robert P.").targetFreq(0).notApproved().comment("für Testzwecke").build(),
                candidate().sourceTerm("Bennett, Maxwell R").sourceFreq(0).targetTerm("Bennett, Matthew R.").targetFreq(0).approved().build(),
                candidate().sourceTerm("Imelgeme").sourceFreq(1).targetTerm("Imergeme.").targetFreq(20).notApproved().comment("merged: alter Kommentar").build(),
                candidate().sourceTerm("John, J").sourceFreq(10).targetTerm("John, John").targetFreq(400).build(),
                candidate().sourceTerm("May").sourceFreq(59).targetTerm("May, Karl").targetFreq(60).build(),
                candidate().sourceTerm("Misterl").sourceFreq(4).targetTerm("Meisterl").targetFreq(50).build(),
                candidate().sourceTerm("Moser, W A").sourceFreq(10).targetTerm("Moser, Wilfgang A").targetFreq(11).build(),
                candidate().sourceTerm("Moser, W.- Amadeus").sourceFreq(8).targetTerm("Moser, Wilfgang A").targetFreq(11).build(),
                candidate().sourceTerm("Moser, Wolfgang A.").sourceFreq(1).targetTerm("Moser, W A").targetFreq(10).build(),
                candidate().sourceTerm("Moser, Wolfgang A.").sourceFreq(1).targetTerm("Moser, Wilfgang A").targetFreq(11).build(),
                candidate().sourceTerm("Prefix, Matcht vorname").sourceFreq(1).targetTerm("Prefix, matx V").targetFreq(2).build(),
                candidate().sourceTerm("Roder, Piter").sourceFreq(7).targetTerm("Roder, Peter").targetFreq(657).build(),
                candidate().sourceTerm("mann tHömas").sourceFreq(1).targetTerm("Mann, ThÖmas").targetFreq(670).notApproved().comment("alte abgelehnte Korrektur => nicht automatisch").build());


        assertEquals(expectedCorrectionCandidates, correctionCandidateExcelFormat.readCorrectionCandidates());

        List<CorrectionCandidate> expectedAutomaticCorrections = Lists.newArrayList(
                candidate().sourceTerm(" Mann+ , Thömas.").sourceFreq(669).targetTerm("Mann, ThÖmas").targetFreq(670).approved().build(),
                candidate().sourceTerm("Imergeme").sourceFreq(11).targetTerm("Imergeme.").targetFreq(20).approved().build(),
                candidate().sourceTerm("Moser, W.-A").sourceFreq(2).targetTerm("Moser, W A").targetFreq(10).approved().build(),
                candidate().sourceTerm("Moser, W.-A").sourceFreq(2).targetTerm("Moser, W.-A.").targetFreq(2).approved().build(),
                candidate().sourceTerm("Moser, W.-A.").sourceFreq(2).targetTerm("Moser, W A").targetFreq(10).approved().build(),
                candidate().sourceTerm("Moser, W.-A").sourceFreq(2).targetTerm("Moser, Wilfgang A").targetFreq(11).approved().comment("-DUPLIKAT-").build(),
                candidate().sourceTerm("Moser, W.-A.").sourceFreq(2).targetTerm("Moser, Wilfgang A").targetFreq(11).approved().comment("-DUPLIKAT-").build(),
                candidate().sourceTerm("imergeme").sourceFreq(1).targetTerm("Imergeme.").targetFreq(20).approved().comment("alte aut. Korrektur").build()); //merged new with old correction and marked as automatic correction (Y, was empty status before);

        assertEquals(expectedAutomaticCorrections, correctionCandidateExcelFormat.readAutomaticCorrections());
    }


    @Test
    public void createCorrectionsFromScratch() throws Exception {
        File file = vlbCorrectionTool.addNewCorrectionsFromIndex(vlbFileInput, VlbCorrectionType.AUTHORS, "/tmp/dict_fold_integration_test_output_without_file.xlsx", null);
        CorrectionCandidateExcelFormat correctionCandidateExcelFormat = new CorrectionCandidateExcelFormat(file, null);

        List<CorrectionCandidate> expectedCorrectionCandidates = Lists.newArrayList(
                candidate().sourceTerm("Imelgeme").sourceFreq(1).targetTerm("Imergeme.").targetFreq(20).build(),
                candidate().sourceTerm("John, J").sourceFreq(10).targetTerm("John, John").targetFreq(400).build(),
                candidate().sourceTerm("May").sourceFreq(59).targetTerm("May, Karl").targetFreq(60).build(),
                candidate().sourceTerm("Misterl").sourceFreq(4).targetTerm("Meisterl").targetFreq(50).build(),
                candidate().sourceTerm("Moser, W A").sourceFreq(10).targetTerm("Moser, Wilfgang A").targetFreq(11).build(),
                candidate().sourceTerm("Moser, W.- Amadeus").sourceFreq(8).targetTerm("Moser, Wilfgang A").targetFreq(11).build(),
                candidate().sourceTerm("Moser, Wolfgang A.").sourceFreq(1).targetTerm("Moser, W A").targetFreq(10).build(),
                candidate().sourceTerm("Moser, Wolfgang A.").sourceFreq(1).targetTerm("Moser, Wilfgang A").targetFreq(11).build(),
                candidate().sourceTerm("Prefix, Matcht vorname").sourceFreq(1).targetTerm("Prefix, matx V").targetFreq(2).build(),
                candidate().sourceTerm("Roder, Piter").sourceFreq(7).targetTerm("Roder, Peter").targetFreq(657).build());



        assertEquals(expectedCorrectionCandidates, correctionCandidateExcelFormat.readCorrectionCandidates());

        List<CorrectionCandidate> expectedAutomaticCorrections = Lists.newArrayList(
                candidate().sourceTerm(" Mann+ , Thömas.").sourceFreq(669).targetTerm("Mann, ThÖmas").targetFreq(670).approved().build(),
                candidate().sourceTerm("Imergeme").sourceFreq(11).targetTerm("Imergeme.").targetFreq(20).approved().build(),
                candidate().sourceTerm("Moser, W.-A").sourceFreq(2).targetTerm("Moser, W A").targetFreq(10).approved().build(),
                candidate().sourceTerm("Moser, W.-A").sourceFreq(2).targetTerm("Moser, W.-A.").targetFreq(2).approved().build(),
                candidate().sourceTerm("Moser, W.-A.").sourceFreq(2).targetTerm("Moser, W A").targetFreq(10).approved().build(),
                candidate().sourceTerm("imergeme").sourceFreq(1).targetTerm("Imergeme.").targetFreq(20).approved().build(),
                candidate().sourceTerm("mann tHömas").sourceFreq(1).targetTerm("Mann, ThÖmas").targetFreq(670).approved().build(),
                candidate().sourceTerm("Moser, W.-A").sourceFreq(2).targetTerm("Moser, Wilfgang A").targetFreq(11).approved().comment("-DUPLIKAT-").build(),
                candidate().sourceTerm("Moser, W.-A.").sourceFreq(2).targetTerm("Moser, Wilfgang A").targetFreq(11).approved().comment("-DUPLIKAT-").build());

        assertEquals(expectedAutomaticCorrections, correctionCandidateExcelFormat.readAutomaticCorrections());
    }


    private CorrectionCandidateTestBuilder candidate() {
        return new CorrectionCandidateTestBuilder();
    }


    private File getResource(String fileName) {
        return new File(getClass().getResource(fileName).getFile());
    }
}