package com.languagetools.dictfold.vlb;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;

import static org.junit.Assert.*;


public class VlbCorrectionToolTest {


    private VlbCorrectionTool vlbCorrectionTool;


    @Before
    public void init() throws Exception {
        vlbCorrectionTool = new VlbCorrectionTool(new PropertiesConfiguration("config.properties"));
    }


    @Test(expected = NullPointerException.class)
    public void nullFileThrowsForWriteCorrectionChains(){
        vlbCorrectionTool.writeCorrectionChainsInSolrSynonymFormat(new StringWriter(), null);
    }


    @Test(expected = NullPointerException.class)
    public void nullWriterThrowsForWriteCorrectionChains(){
        vlbCorrectionTool.writeCorrectionChainsInSolrSynonymFormat(null, getFileFromResource("validCorrectionCandidates.xlsx"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void fileWithWrongFormatThrowsForWriteCorrectionChains(){
        vlbCorrectionTool.writeCorrectionChainsInSolrSynonymFormat(new StringWriter(), getFileFromResource("missingHeader.xlsx"));
    }


    @Test
    public void writeCorrectionChains() throws Exception {
        Writer writer = new StringWriter();
        String resourceName = "correctionsToApply.xlsx";
        vlbCorrectionTool.writeCorrectionChainsInSolrSynonymFormat(writer,
                getFileFromResource(resourceName));
        assertEquals("föo,fii,fu,föö,foo,fou\nfop,foo\nbaz,Foo bar,bar\n", writer.toString());
    }


    private File getFileFromResource(String resourceName) {
        return new File(getClass().getResource(resourceName).getFile());
    }
}