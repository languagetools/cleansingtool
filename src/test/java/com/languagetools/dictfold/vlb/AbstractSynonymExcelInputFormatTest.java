package com.languagetools.dictfold.vlb;


import com.google.common.collect.Lists;
import com.languagetools.dictfold.ExcelFormat;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AbstractSynonymExcelInputFormatTest {


    @Rule
    public ExpectedException thrown = ExpectedException.none();
    protected ExcelFormat synonymExcelInputFormat;
    protected List<IndexSynonym> synonyms;

    protected void anExceptionIsThrown(Class<? extends Exception> exception) {
        thrown.expect(exception);
    }


    protected void thenTheOutputIndexSynonymsAre(IndexSynonym... expectedSynonyms){
        assertEquals(Lists.newArrayList(expectedSynonyms), synonyms);
    }


    protected File getFileFromResource(String resourceName) {
        return new File(getClass().getResource(resourceName).getFile());
    }


}
