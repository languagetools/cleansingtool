package com.languagetools.dictfold.vlb;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


public class SynonymExcelInputFormatTest extends AbstractSynonymExcelInputFormatTest{


    @Test
    public void nullFileThrows() {
        anExceptionIsThrown(NullPointerException.class);
        givenAnSynoymExcel(null);
    }


    @Test
    public void nonExistingSynonymsThrow() throws Exception {
        givenAnNonExistingSynoymExcel();
        anExceptionIsThrown(FileNotFoundException.class);
        whenAColumIsRead(SynonymExcelInputFormat.ColumnName.TITLE);
    }


    @Test
    public void brokenSynonymHeaderThrows() throws Exception {
        givenAnSynoymExcel("incorrectHeaderSynonyms.xlsx");
        anExceptionIsThrown(IllegalArgumentException.class);
        whenAColumIsRead(SynonymExcelInputFormat.ColumnName.DESCRIPTION);
    }


    @Test
    public void missingSynonymHeaderThrows() throws Exception {
        givenAnSynoymExcel("missingHeaderSynonyms.xlsx");
        anExceptionIsThrown(IllegalArgumentException.class);
        whenAColumIsRead(SynonymExcelInputFormat.ColumnName.TITLE);
    }

    @Test
    public void readSynonymsSuccessfullyNewExcelFormat() throws Exception {
        givenAnSynoymExcel("validSynonyms.xlsx");
        whenAColumIsRead(SynonymExcelInputFormat.ColumnName.TITLE);
        thenTheOutputIndexSynonymsAre(IndexSynonym.of("Abbruchbirne", "Abrissbirne"), IndexSynonym.of("Abendmusik", "Serenade"),
                IndexSynonym.of("alle", "Bereiche"));
    }


    @Test
    public void readSynonymsSuccessfullyOldExcelFormat() throws Exception {
        givenAnSynoymExcel("validSynonyms.xls");
        whenAColumIsRead(SynonymExcelInputFormat.ColumnName.TITLE);
        thenTheOutputIndexSynonymsAre(IndexSynonym.of("Abbruchbirne", "Abrissbirne"), IndexSynonym.of("alle", "Bereiche"));
    }


    @Test
    public void synonymsAreConsideredSelectedIfNotBlank() throws Exception {
        givenAnSynoymExcel("validSynonyms.xlsx");
        whenAColumIsRead(SynonymExcelInputFormat.ColumnName.DESCRIPTION);
        thenTheOutputIndexSynonymsAre(IndexSynonym.of("Abbruchbirne", "Abrissbirne"), IndexSynonym.of("Abendmahl", "Brotbrechen"),
                IndexSynonym.of("alle", "Bereiche"));
    }


    @Test
    public void readEmptyColumnSuccessfully() throws Exception {
        givenAnSynoymExcel("validSynonyms.xlsx");
        whenAColumIsRead(SynonymExcelInputFormat.ColumnName.PUBLISHER_TAGS);
        thenTheOutputIndexSynonymsAre(IndexSynonym.of("alle", "Bereiche"));
    }


    @Test
    public void requestedNullColumnThrows() throws Exception {
        givenAnSynoymExcel("validSynonyms.xlsx");
        anExceptionIsThrown(NullPointerException.class);
        whenAColumIsRead(null);
    }


    @Test
    public void requestedUnknownColumnThrows() throws Exception {
        givenAnSynoymExcel("validSynonyms.xlsx");
        anExceptionIsThrown(IllegalArgumentException.class);
        whenAColumIsRead(SynonymExcelInputFormat.ColumnName.UNKNOWN);
    }


    @Test
    public void requestedSynonymTermColumnThrows() throws Exception {
        givenAnSynoymExcel("validSynonyms.xlsx");
        anExceptionIsThrown(IllegalArgumentException.class);
        whenAColumIsRead(SynonymExcelInputFormat.ColumnName.SYNONYM_SOURCE);
    }


    private void whenAColumIsRead(SynonymExcelInputFormat.ColumnName columnName) throws IOException, InvalidFormatException {
        synonyms = ((SynonymExcelInputFormat) synonymExcelInputFormat).readSynonymsOfType(columnName);
    }


    private void givenAnSynoymExcel(String synonymsResource) {
        File fileFromResource = synonymsResource == null ? null : getFileFromResource(synonymsResource);
        synonymExcelInputFormat = new SynonymExcelInputFormat(fileFromResource);
    }

    private void givenAnNonExistingSynoymExcel() {
        synonymExcelInputFormat = new SynonymExcelInputFormat(new File("doesnotexist"));
    }


}