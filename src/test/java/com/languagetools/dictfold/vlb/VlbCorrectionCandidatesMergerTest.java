package com.languagetools.dictfold.vlb;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.languagetools.dictfold.*;
import com.languagetools.dictfold.vlb.VlbCorrectionCandidatesMerger;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class VlbCorrectionCandidatesMergerTest {

    private VlbCorrectionCandidatesMerger merger;


    @Before
    public void initMerger() {
        merger = new VlbCorrectionCandidatesMerger();
    }


    @Test(expected = NullPointerException.class)
    public void throwIfOldCandidatesAreNull() throws Exception {
        merger.mergeCorrections(null, Collections.<CorrectionCandidate>emptyList());
    }


    @Test(expected = NullPointerException.class)
    public void throwIfNewCandidatesAreNull() throws Exception {
        merger.mergeCorrections(Collections.<CorrectionCandidate>emptyList(), null);
    }


    @Test
    public void mergeEmptyCandidatesResultsInEmptyMergeResult() throws Exception {
        Collection<CorrectionCandidate> correctionCandidates =
                merger.mergeCorrections(Collections.<CorrectionCandidate>emptyList(), Collections.<CorrectionCandidate>emptyList());
        assertTrue(correctionCandidates.isEmpty());
    }


    @Test
    public void matchIsBlankSensitive() throws Exception {
        CorrectionCandidate oldCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm(" foo.+").build();
        CorrectionCandidate newCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("foo.+").build();
        verifySizeOfMergedCorrections(2, Lists.newArrayList(newCandidate), Lists.newArrayList(oldCandidate));
    }


    @Test
    public void matchIsCaseSensitive() throws Exception {
        CorrectionCandidate oldCandidate = new CorrectionCandidateTestBuilder().sourceTerm("Foo").targetTerm("foo.+").build();
        CorrectionCandidate newCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("foo.+").build();
        verifySizeOfMergedCorrections(2, Lists.newArrayList(newCandidate), Lists.newArrayList(oldCandidate));
    }


    @Test
    public void matchDoesIgnoreFoldersAndStatusAndFrequencies() throws Exception {
        List<CorrectionCandidate> oldCorrectionCandidates =
                Lists.newArrayList(new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("foo.+").addFolder("EX").approved().build());
        List<CorrectionCandidate> newCorrectionCandidates =
                Lists.newArrayList(new CorrectionCandidateTestBuilder().sourceTerm("foo").
                                              sourceFreq(4).targetTerm("foo.+").targetFreq(9).addFolder("LS").notApproved().build());
        verifySizeOfMergedCorrections(1, newCorrectionCandidates, oldCorrectionCandidates);
    }


    @Test
    public void usedFoldersAreMerged() throws Exception {
        CorrectionCandidate oldCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("foo.+").addFolder("EX").addFolder("LS").build();
        CorrectionCandidate newCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("foo.+").addFolder("LS").addFolder("FN").build();
        verifyMergedCandidates(Sets.newHashSet(new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("foo.+")
                        .addFolder("LS").addFolder("EX").addFolder("FN").build()),
                Lists.newArrayList(oldCandidate), Lists.newArrayList(newCandidate));
    }


    @Test
    public void takeNewCorrectionsAttributesInMerge() throws Exception {
        CorrectionCandidate oldCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("foo.+").comment("keepMe").build();
        CorrectionCandidate newCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").sourceFreq(2).targetTerm("foo.+").targetFreq(3).build();
        verifyMergedCandidates(Sets.newHashSet(new CorrectionCandidateTestBuilder().sourceTerm("foo").sourceFreq(2)
                                                .targetTerm("foo.+").targetFreq(3).comment("keepMe").build()),
                                Lists.newArrayList(oldCandidate), Lists.newArrayList(newCandidate));
    }


    @Test
     public void useNewCorrectionStatusForOpenOldCorrection() throws Exception {
        CorrectionCandidate oldCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("foo.+").build();
        CorrectionCandidate newCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("foo.+").approved().build();
        verifyMergedCandidates(Sets.newHashSet(new CorrectionCandidateTestBuilder().sourceTerm("foo")
                .targetTerm("foo.+").approved().build()), Lists.newArrayList(oldCandidate), Lists.newArrayList(newCandidate));
    }


    @Test
    public void useOldCorrectionStatusIfNotOpen() throws Exception {
        CorrectionCandidate oldCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("foo.+").notApproved().build();
        CorrectionCandidate newCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("foo.+").approved().build();
        verifyMergedCandidates(Sets.newHashSet(new CorrectionCandidateTestBuilder().sourceTerm("foo")
                .targetTerm("foo.+").notApproved().build()), Lists.newArrayList(oldCandidate), Lists.newArrayList(newCandidate));
    }


    @Test
    public void takeNewUnmatchedCorrectionAsItIs() throws Exception {
        List<CorrectionCandidate> newCandidates = Lists.newArrayList(new CorrectionCandidateTestBuilder().sourceTerm("foo").
                sourceFreq(2).targetTerm("foo.+").sourceFreq(3).comment("com").addFolder("EX").build());
        verifyMergedCandidates(newCandidates, Collections.<CorrectionCandidate>emptyList(), newCandidates);
    }


    @Test
    public void addZeroFrequenciesToOldUnmatchedCorrection() throws Exception {
        CorrectionCandidate oldCorrectionCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").
                sourceFreq(2).targetTerm("foo.+").targetFreq(3).approved().comment("com").addFolder("EX").build();
        verifyMergedCandidates(
                Sets.newHashSet(new CorrectionCandidateTestBuilder().sourceTerm("foo").sourceFreq(0).targetTerm("foo.+")
                        .targetFreq(0).approved().comment("com").addFolder("EX").build()),
                Lists.newArrayList(oldCorrectionCandidate),  Collections.<CorrectionCandidate>emptyList());
    }


    private void verifySizeOfMergedCorrections(int expectedSize, List<CorrectionCandidate> newCorrectionCandidates,
                                               List<CorrectionCandidate> oldCorrectionCandidates) {
        Collection<CorrectionCandidate> correctionCandidates =
                merger.mergeCorrections(oldCorrectionCandidates, newCorrectionCandidates);
        assertEquals(expectedSize, correctionCandidates.size());
    }


    private void verifyMergedCandidates(Collection<CorrectionCandidate> mergedCandidates,
                                        List<CorrectionCandidate> oldCorrectionCandidates,
                                        List<CorrectionCandidate> newCorrectionCandidates) {
        Collection<CorrectionCandidate> correctionCandidates =
                merger.mergeCorrections(oldCorrectionCandidates, newCorrectionCandidates);
        assertEquals(Sets.newHashSet(mergedCandidates), correctionCandidates);
    }

}
