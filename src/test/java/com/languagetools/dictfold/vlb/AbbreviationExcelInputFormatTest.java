package com.languagetools.dictfold.vlb;

import com.google.common.collect.Lists;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class AbbreviationExcelInputFormatTest extends AbstractSynonymExcelInputFormatTest{


    @Test
    public void nullFileThrows() {
        anExceptionIsThrown(NullPointerException.class);
        givenAnAbbreviationExcel(null);
    }


    @Test
    public void nonExistingAbbreviationThrow() throws Exception {
        givenAnNonExistingAbbreviationExcel();
        anExceptionIsThrown(FileNotFoundException.class);
        whenAbbrevationIsRead();
    }


    @Test
    public void brokenAbbreviationHeaderThrows() throws Exception {
        givenAnAbbreviationExcel("incorrectHeaderAbbreviations.xls");
        anExceptionIsThrown(IllegalArgumentException.class);
        whenAbbrevationIsRead();
    }


    @Test
    public void missingAbbreviationHeaderThrows() throws Exception {
        givenAnAbbreviationExcel("missingHeaderAbbreviations.xls");
        anExceptionIsThrown(IllegalArgumentException.class);
        whenAbbrevationIsRead();
    }

    @Test
    public void readSynonymsSuccessfullyNewExcelFormat() throws Exception {
        givenAnAbbreviationExcel("validAbbreviations.xlsx");
        whenAbbrevationIsRead();
        thenTheOutputIndexSynonymsAre(IndexSynonym.of("Abb.", "Abbildung"), IndexSynonym.of("Abs.", "Absatz"),
                IndexSynonym.of("Abschn.", "Abschnitt"));
    }


    @Test
    public void readSynonymsSuccessfullyOldExcelFormat() throws Exception {
        givenAnAbbreviationExcel("validAbbreviations.xls");
        whenAbbrevationIsRead();
        thenTheOutputIndexSynonymsAre(IndexSynonym.of("Abb.", "Abbildung"), IndexSynonym.of("Abs.", "Absatz"),
                IndexSynonym.of("Abschn.", "Abschnitt"));
    }


    private void whenAbbrevationIsRead() throws IOException, InvalidFormatException {
        synonyms = ((AbbreviationExcelInputFormat) synonymExcelInputFormat).readAbbreviations();
    }


    private void givenAnAbbreviationExcel(String synonymsResource) {
        File fileFromResource = synonymsResource == null ? null : getFileFromResource(synonymsResource);
        synonymExcelInputFormat = new AbbreviationExcelInputFormat(fileFromResource);
    }

    private void givenAnNonExistingAbbreviationExcel() {
        synonymExcelInputFormat = new AbbreviationExcelInputFormat(new File("doesnotexist"));
    }

}