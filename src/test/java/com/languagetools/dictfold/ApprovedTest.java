package com.languagetools.dictfold;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ApprovedTest {

    @Test
    public void buildOfNullResultsInDefault() {
        assertEquals(CorrectionCandidate.Approved.OPEN, CorrectionCandidate.Approved.of(null));
    }

    @Test
    public void buildOfUnknownResultsInDefault() {
        assertEquals(CorrectionCandidate.Approved.OPEN, CorrectionCandidate.Approved.of("unbekannt"));
    }

    @Test
    public void buildOfEmptStringResultsInOpen() {
        assertEquals(CorrectionCandidate.Approved.OPEN, CorrectionCandidate.Approved.of(""));
    }

    @Test
    public void buildOfKnownResultsInOpen() {
        assertEquals(CorrectionCandidate.Approved.YES, CorrectionCandidate.Approved.of("Y"));
    }

    @Test
    public void buildOfIsCaseInsensitive() {
        assertEquals(CorrectionCandidate.Approved.NO, CorrectionCandidate.Approved.of("n"));
    }

    @Test
    public void buildOfTrims() {
        assertEquals(CorrectionCandidate.Approved.NO, CorrectionCandidate.Approved.of(" n "));
    }

    @Test
    public void toStringUsesRepresentation() {
        assertEquals("J", CorrectionCandidate.Approved.YES.toString());
    }
}