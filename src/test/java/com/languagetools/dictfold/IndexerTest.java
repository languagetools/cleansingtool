package com.languagetools.dictfold;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class IndexerTest {


    @Test
    public void normalizeNullToEmpty(){
        assertEquals("", Indexer.normalizeErasingSpecialChars(null));
    }


    @Test
    public void normalizeBlankStringToEmpty(){
        assertEquals("", Indexer.normalizeErasingSpecialChars("   "));
    }


    @Test
    public void normalizeStrippingWhitespaces(){
        assertEquals("", Indexer.normalizeErasingSpecialChars(" \t\n        "));
    }


    @Test
    public void normalizeErasingSpecialCharacters(){
        assertEquals("alles mussweg", Indexer.normalizeErasingSpecialChars(" .Alles;+)  muss.'-weg "));
    }


    @Test
    public void normalizeSubstituteSpecialCharactersWithSpaces(){
        assertEquals("alles muss weg", Indexer.normalizeErasingSpecialChars(" .Alles;+)muss.'-weg ", true));
    }


    @Test
    public void normalizeLowercasingUmlautsToo(){
        assertEquals("üöää", Indexer.normalizeErasingSpecialChars("üÖÄä", true));
    }


}