package com.languagetools.dictfold;


import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import java.util.List;

public class CorrectionCandidateTestBuilder {


    private String sourceTerm;
    private int sourceFrequency = 1;
    private String targetTerm;
    private int targetFrequency = 1;
    private CorrectionCandidate.Approved correctionStatus = CorrectionCandidate.Approved.OPEN;
    private String comment = "";
    private List<String> usedFolders = Lists.newArrayList();


    public CorrectionCandidateTestBuilder sourceTerm(String sourceTerm) {
        this.sourceTerm = sourceTerm;
        return this;
    }


    public CorrectionCandidateTestBuilder targetTerm(String targetTerm) {
        this.targetTerm = targetTerm;
        return this;
    }


    public CorrectionCandidateTestBuilder sourceFreq(int sourceFrequency) {
        this.sourceFrequency = sourceFrequency;
        return this;
    }


    public CorrectionCandidateTestBuilder targetFreq(int targetFrequency) {
        this.targetFrequency = targetFrequency;
        return this;
    }


    public CorrectionCandidateTestBuilder approved() {
        this.correctionStatus = CorrectionCandidate.Approved.YES;
        return this;
    }


    public CorrectionCandidateTestBuilder notApproved() {
        this.correctionStatus = CorrectionCandidate.Approved.NO;
        return this;
    }


    public CorrectionCandidateTestBuilder addFolder(String folder) {
        this.usedFolders.add(folder);
        return this;
    }


    public CorrectionCandidateTestBuilder comment(String comment) {
        this.comment = comment;
        return this;
    }


    public CorrectionCandidate build(){
        return CorrectionCandidate.of(TermFrequency.of(sourceTerm, sourceFrequency),
                                      TermFrequency.of(targetTerm, targetFrequency),
                                      correctionStatus, comment, Iterables.toArray(usedFolders, String.class));
    }

}
