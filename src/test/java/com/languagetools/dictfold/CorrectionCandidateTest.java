package com.languagetools.dictfold;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;


public class CorrectionCandidateTest {

    @Test
    public void buildOfCorrectionStatusDefault(){
        CorrectionCandidate candidate = CorrectionCandidate.of(TermFrequency.of("fii", 1), TermFrequency.of("foo", 2), "EX");
        assertEquals(CorrectionCandidate.Approved.OPEN, candidate.getCorrectionStatus());
    }


    @Test
    public void buildOfCorrectionCommentDefault(){
        CorrectionCandidate candidate = CorrectionCandidate.of(TermFrequency.of("fii", 1), TermFrequency.of("foo", 2), "EX");
        assertEquals("", candidate.getComment());
    }


    @Test
    public void buildOfNoUsedFolder(){
        CorrectionCandidate candidate = new CorrectionCandidateTestBuilder().sourceTerm("fii").targetTerm("foo").notApproved().build();
        assertEquals(Collections.emptyList(), candidate.getUsedFolders());
    }

    @Test
    public void buildOfAllSet(){
        CorrectionCandidate candidate = new CorrectionCandidateTestBuilder().sourceTerm("fii").targetTerm("foo").addFolder("EX").addFolder("LS").build();
        assertEquals(Lists.newArrayList("EX", "LS"), candidate.getUsedFolders());
    }

    @Test
     public void approveYetApprovedCandidate(){
        CorrectionCandidate candidate = new CorrectionCandidateTestBuilder().sourceTerm("fii").targetTerm("foo").approved().build();
        candidate.approve();
        assertEquals(CorrectionCandidate.Approved.YES, candidate.getCorrectionStatus());
    }

    @Test
    public void approveCandidate(){
        CorrectionCandidate candidate = new CorrectionCandidateTestBuilder().sourceTerm("fii").targetTerm("foo").notApproved().build();
        candidate.approve();
        assertEquals(CorrectionCandidate.Approved.YES, candidate.getCorrectionStatus());
    }
}