package com.languagetools.dictfold;

import org.apache.lucene.document.Document;
import org.junit.Test;

import static org.junit.Assert.*;

public class IndexingTest extends AbstractIndexTest {


    @Test
    public void createDocument() {
        Document doc = indexer.createDocFromTermFrequency(TermFrequency.of("foo", 125));
        assertEquals("foo", doc.get("term"));
        assertEquals(125, doc.getField("freq").numericValue());
    }


    @Test
    public void createIndexIndexesAllDocuments() {
        int numDocs = createTestIndex();
        assertEquals(9, numDocs);
    }

}
