package com.languagetools.dictfold.configuration;


import com.languagetools.dictfold.Indexer;
import org.apache.commons.configuration.Configuration;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class IndexerFactoryTest extends ConfigurationTestBase {


    private Indexer indexer;


    @Test(expected = NullPointerException.class)
    public void nullInputResultsInException(){
        new IndexerFactory(null);
    }


    @Test
    public void whenIndexDirectoryIsNotProvidedATempDirectoryIsUsed() throws Exception {
        givenFactoryFromConfiguration(new ConfigurationBuilder().build());
        whenCreateIsCalled();
        thenTheIndexerIsNotNull();
    }


    private void thenTheIndexerIsNotNull() {
        assertNotNull(indexer);
    }


    private void whenCreateIsCalled() {
        indexer = ((IndexerFactory)configurationFactory).createIndexer();
    }




    @Override
    protected IndexerFactory createFactory(Configuration configuration) {
            return new IndexerFactory(configuration);

    }
}
