package com.languagetools.dictfold.configuration;


import com.google.common.collect.Lists;
import com.languagetools.dictfold.*;
import com.languagetools.dictfold.match.AbstractFolder;
import com.languagetools.dictfold.match.exact.ExactMatchFolder;
import com.languagetools.dictfold.match.exact.ExtendedExactMatchFolder;
import com.languagetools.dictfold.match.fuzzy.LevenshteinFolder;
import com.languagetools.dictfold.match.name.LastNameMatchFolder;
import com.languagetools.dictfold.match.fuzzy.NameLevenshteinFolder;
import com.languagetools.dictfold.match.name.NameVariantMatchFolder;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConversionException;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FoldingExecutorFactoryTest extends ConfigurationTestBase {

    private FoldingExecutor foldingExecutor;


    @Test(expected = NullPointerException.class)
    public void nullInputResultsInException() {
        new FoldingExecutorFactory(null);
    }


    @Test
    public void emptyConfigurationCreatesDefaultFoldingExecutor() throws Exception {
        givenFactoryFromConfiguration(new ConfigurationBuilder().build());
        whenCreateIsCalled();
        thenFoldingExecutorIsCreated(new FoldingExecutorBuilder().withNoFolders().build());
    }


    @Test
    public void separatorCanBeChanged() throws Exception {
        givenFactoryFromConfiguration(new ConfigurationBuilder().withSeparator(";").build());
        whenCreateIsCalled();
        thenFoldingExecutorIsCreated(new FoldingExecutorBuilder().withSeparator(";").build());
    }


    @Test
    public void allFoldersPropertyCreatesFoldersWithDefaults() throws Exception {
        givenFactoryFromConfiguration(new ConfigurationBuilder().withAllFoldersEnabled().build());
        whenCreateIsCalled();
        thenFoldingExecutorIsCreated(new FoldingExecutorBuilder().withAllDefaultFolders().build());
    }


    @Test
    public void allFoldersExceptOneShallBeUsed() throws Exception {
        givenFactoryFromConfiguration(new ConfigurationBuilder().withAllFoldersEnabled().withLevenshteinFolder(false).build());
        whenCreateIsCalled();
        thenFoldingExecutorIsCreated(foldingExecutorWithAllFoldersExceptLevenshtein());
    }


    @Test
    public void onlyOneFolderShallBeUsed() throws Exception {
        givenFactoryFromConfiguration(new ConfigurationBuilder().withExactMatchFolder("true").build());
        whenCreateIsCalled();
        thenFoldingExecutorIsCreated(new FoldingExecutorBuilder().withExactMatchFolder().build());
    }


    @Test
    public void removingUnexistentFolderHasNoEffect() throws Exception {
        givenFactoryFromConfiguration(new ConfigurationBuilder().withExactMatchFolder("false").build());
        whenCreateIsCalled();
        thenFoldingExecutorIsCreated(new FoldingExecutorBuilder().withNoFolders().build());
    }


    @Test(expected = ConversionException.class)
    public void folderPropertyCannotBeParsed() throws Exception {
        givenFactoryFromConfiguration(new ConfigurationBuilder().withExactMatchFolder("Fasle").build());
        whenCreateIsCalled();
    }


    @Test
    public void folderHasNoBeEnabledToConfigureFolderSpecificProperties() throws Exception {
        givenFactoryFromConfiguration(new ConfigurationBuilder().withExactMatchFolder("true").withLevenshteinFrequencyThreshold(100).build());
        whenCreateIsCalled();
        thenFoldingExecutorIsCreated(new FoldingExecutorBuilder().withExactMatchFolder().build());
    }


    @Test
    public void levenshteinFrequencyThresholdCanBeConfigured() throws Exception {
        givenFactoryFromConfiguration(new ConfigurationBuilder().withLevenshteinFolder(true).withLevenshteinFrequencyThreshold(100).build());
        whenCreateIsCalled();
        thenFoldingExecutorIsCreated(new FoldingExecutorBuilder().withLevenstheinFolderOfFrequencyThreshold(100).build());
    }


    @Test
    public void levenshteinMinimalTermLengthCanBeConfigured() throws Exception {
        givenFactoryFromConfiguration(new ConfigurationBuilder().withLevenshteinFolder(true).withLevenshteinMinimalTermLength(5).build());
        whenCreateIsCalled();
        thenFoldingExecutorIsCreated(new FoldingExecutorBuilder().withLevenstheinFolderOfMinTermLength(5).build());
    }


    @Test
    public void levenshteinMaxEditDistanceCanBeConfigured() throws Exception {
        givenFactoryFromConfiguration(new ConfigurationBuilder().withLevenshteinFolder(true).withLevenshteinMaxEdit(12).build());
        whenCreateIsCalled();
        thenFoldingExecutorIsCreated(new FoldingExecutorBuilder().withLevenstheinFolderOfMaxEdit(12).build());
    }


    @Test
    public void levenshteinPrefixLengthCanBeConfigured() throws Exception {
        givenFactoryFromConfiguration(new ConfigurationBuilder().withLevenshteinFolder(true).withLevenshteinPrefixLength(16).build());
        whenCreateIsCalled();
        thenFoldingExecutorIsCreated(new FoldingExecutorBuilder().withLevenstheinFolderOfPrefixLength(16).build());
    }


    public FoldingExecutor foldingExecutorWithAllFoldersExceptLevenshtein() throws FileNotFoundException {
        return new FoldingExecutorBuilder().withExactMatchFolder().withExtendedExactMatchFolder().withLastNameMatchFolder().withNameVariantMatchFolder()
                .withDefaultNamesLevenstheinFolder().build();
    }


    private void whenCreateIsCalled() throws Exception {
        foldingExecutor = ((FoldingExecutorFactory)configurationFactory).createFoldingExecutor();
    }


    private void thenFoldingExecutorIsCreated(FoldingExecutor expectedFoldingExecutor) {
        assertEquals(expectedFoldingExecutor, foldingExecutor);
    }


    @Override
    protected ConfigurationFactory createFactory(Configuration configuration) {
        return new FoldingExecutorFactory(configuration);
    }


    private class FoldingExecutorBuilder {

        private int defaultMinTermLength = 7;
        private int defaultFrequencyThreshold = 10;
        private int defaultMaxEdit = 1;
        private int defaultPrefixLength = 1;

        private List<AbstractFolder> folders = Lists.newArrayList();
        private File indexDirectory = null;
        private String separator = "\t";

        public FoldingExecutorBuilder withSeparator(String separator) {
            this.separator = separator;
            return this;
        }


        public FoldingExecutorBuilder withNoFolders() {
            return this;
        }


        public FoldingExecutorBuilder withAllDefaultFolders() {
            withDefaultLevenstheinFolder();
            withDefaultNamesLevenstheinFolder();
            withExactMatchFolder();
            withExtendedExactMatchFolder();
            withLastNameMatchFolder();
            withNameVariantMatchFolder();
            return this;
        }


        private FoldingExecutorBuilder withLastNameMatchFolder() {
            folders.add(new LastNameMatchFolder(indexDirectory));
            return this;
        }


        private FoldingExecutorBuilder withNameVariantMatchFolder() {
            folders.add(new NameVariantMatchFolder(indexDirectory));
            return this;

        }


        private FoldingExecutorBuilder withExactMatchFolder() {
         folders.add(new ExactMatchFolder(indexDirectory));
            return this;
        }

        private FoldingExecutorBuilder withExtendedExactMatchFolder() {
            folders.add(new ExtendedExactMatchFolder(indexDirectory));
            return this;
        }


        public FoldingExecutorBuilder withDefaultNamesLevenstheinFolder() {
            folders.add(new NameLevenshteinFolder(indexDirectory, new LevenshteinConfiguration.Builder().applyToFullNameCandidates(true).build()));
            return this;
        }


        private FoldingExecutorBuilder withLevenshteinFolder(int minTermLength, int levenstheinFrequencyThreshold, int maxEdits, int prefixLength) {
            folders.add(new LevenshteinFolder(indexDirectory, new LevenshteinConfiguration.Builder().minimalTermLength(minTermLength)
                    .frequencyThreshold(levenstheinFrequencyThreshold).maximalEditDistance(maxEdits).prefixLength(prefixLength).build()));
            return this;
        }


        public FoldingExecutorBuilder withDefaultLevenstheinFolder() {
            return withLevenshteinFolder(defaultMinTermLength, defaultFrequencyThreshold, defaultMaxEdit, defaultPrefixLength);
        }


        public FoldingExecutorBuilder withLevenstheinFolderOfFrequencyThreshold(int frequencyThreshold) {
            return withLevenshteinFolder(defaultMinTermLength, frequencyThreshold, defaultMaxEdit, defaultPrefixLength);
        }


        public FoldingExecutorBuilder withLevenstheinFolderOfMinTermLength(int minTermLength) {
            return withLevenshteinFolder(minTermLength, defaultFrequencyThreshold, defaultMaxEdit, defaultPrefixLength);

        }


        public FoldingExecutorBuilder withLevenstheinFolderOfMaxEdit(int maxEdit) {
            return withLevenshteinFolder(defaultMinTermLength, defaultFrequencyThreshold, maxEdit, defaultPrefixLength);
        }


        public FoldingExecutorBuilder withLevenstheinFolderOfPrefixLength(int prefixLength) {
            return withLevenshteinFolder(defaultMinTermLength, defaultFrequencyThreshold, defaultMaxEdit, prefixLength);
        }



        public FoldingExecutor build() throws FileNotFoundException {
            return new FoldingExecutor(separator, folders);
        }
    }
}