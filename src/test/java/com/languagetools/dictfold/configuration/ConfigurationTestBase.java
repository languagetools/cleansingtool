package com.languagetools.dictfold.configuration;


import com.google.common.collect.Maps;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.MapConfiguration;

import java.util.Map;

public abstract class ConfigurationTestBase {

    protected ConfigurationFactory configurationFactory;


    protected void givenFactoryFromConfiguration(Configuration configuration) throws Exception {
        configurationFactory = createFactory(configuration);
    }


    protected abstract ConfigurationFactory createFactory(Configuration configuration) throws Exception;


    protected class ConfigurationBuilder {

        private Map<String, Object> properties;


        public ConfigurationBuilder() {
            properties = Maps.newHashMap();
        }

        public ConfigurationBuilder withSeparator(String separator){
            properties.put("file.separator", separator);
            return this;
        }


        public ConfigurationBuilder withAllFoldersEnabled() {
            properties.put("folders.enable.all", "true");
            return this;
        }


        public ConfigurationBuilder withLevenshteinFolder(boolean enabled) {
            properties.put("folders.enable.levenshtein", enabled ? "true": "false");
            return this;
        }


        public ConfigurationBuilder withExactMatchFolder(String value) {
            properties.put("folders.enable.exact", value);
            return this;
        }


        public ConfigurationBuilder withLevenshteinFrequencyThreshold(int threshold) {
            properties.put("folders.levenshtein.frequencyThreshold", threshold);
            return this;
        }


        public ConfigurationBuilder withLevenshteinMinimalTermLength(int termLength) {
            properties.put("folders.levenshtein.minimalTermLength", termLength);
            return this;
        }


        public ConfigurationBuilder withLevenshteinMaxEdit(int maxEdit) {
            properties.put("folders.levenshtein.maxEdit", maxEdit);
            return this;
        }


        public ConfigurationBuilder withLevenshteinPrefixLength(int prefixLength) {
            properties.put("folders.levenshtein.prefixLength", prefixLength);
            return this;
        }


        public ConfigurationBuilder withExistingCorrectionFile(String skipCorrectionsFile) {
            properties.put("file.oldCorrections", skipCorrectionsFile);
            return this;
        }


        public ConfigurationBuilder withOutputFile(String outputFile) {
            properties.put("file.output", outputFile);
            return this;
        }


        public Configuration build() {
            return new MapConfiguration(properties);
        }

    }
}
