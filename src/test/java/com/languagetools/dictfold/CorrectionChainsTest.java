package com.languagetools.dictfold;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;


public class CorrectionChainsTest {


    @Test
    public void noCorrectionsWriteEmptyOutput() {
        verifyOutputForCandidates("", Collections.<CorrectionCandidate>emptyList());
    }


    @Test
    public void onlyApprovedCorrectionsAreWritten() {
        verifyOutputForCandidates("bar,foo\n", Lists.newArrayList(new CorrectionCandidateTestBuilder().sourceTerm("ignored")
                .targetTerm("ignored").build(), new CorrectionCandidateTestBuilder().sourceTerm("foo").targetTerm("bar").approved().build()));
    }


    @Test
    public void chainIsBuiltFromSourceAndTarget() {
        ArrayList<CorrectionCandidate> correctionCandidates = Lists.newArrayList(
                new CorrectionCandidateTestBuilder().sourceTerm("bla").targetTerm("blub").approved().build(),
                new CorrectionCandidateTestBuilder().sourceTerm("blub").targetTerm("foi").approved().build(),
                new CorrectionCandidateTestBuilder().sourceTerm("schnu").targetTerm("blub").approved().build(),
                new CorrectionCandidateTestBuilder().sourceTerm("bla").targetTerm("foo").approved().build(),
                new CorrectionCandidateTestBuilder().sourceTerm("bar").targetTerm("bla").approved().build(),
                new CorrectionCandidateTestBuilder().sourceTerm("sep").targetTerm("arat").approved().build());
        verifyOutputForCandidates("bar,bla,blub,foi,foo,schnu\narat,sep\n", correctionCandidates);
    }


    @Test
    public void toBuildChainFrequencyMustMatchToo() {
        ArrayList<CorrectionCandidate> correctionCandidates = Lists.newArrayList(
                new CorrectionCandidateTestBuilder().sourceTerm("bla").targetTerm("foa").targetFreq(2).approved().build(),
                new CorrectionCandidateTestBuilder().sourceTerm("foa").targetTerm("foi").approved().build());
        verifyOutputForCandidates("foa,bla\nfoa,foi\n", correctionCandidates);
    }


    @Test
    public void outputIsSortedByFrequencyFirst(){
        ArrayList<CorrectionCandidate> correctionCandidates = Lists.newArrayList(
                new CorrectionCandidateTestBuilder().sourceTerm("winner").sourceFreq(3).targetTerm("foa").targetFreq(2).approved().build(),
                new CorrectionCandidateTestBuilder().sourceTerm("foa").sourceFreq(2).targetTerm("foi").targetFreq(2).approved().build());
        verifyOutputForCandidates("winner,foa,foi\n", correctionCandidates);
    }


    @Test
    public void commataAreReplacedNormalizingSpaces() {
        ArrayList<CorrectionCandidate> correctionCandidates = Lists.newArrayList(
                new CorrectionCandidateTestBuilder().sourceTerm("winner,  Max").sourceFreq(3).targetTerm("foo,,bar").targetFreq(2).approved().build());
        verifyOutputForCandidates("winner Max,foo bar\n", correctionCandidates);
    }


    private void verifyOutputForCandidates(String expectedOutput, List<CorrectionCandidate> correctionCandidates) {
        StringWriter writer = new StringWriter();
        CorrectionChains.of(correctionCandidates).writeToIndexFormat(writer);
        assertEquals(expectedOutput, writer.toString());
    }
}