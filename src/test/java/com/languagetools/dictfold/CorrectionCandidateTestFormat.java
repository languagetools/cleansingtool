package com.languagetools.dictfold;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

public class CorrectionCandidateTestFormat implements  CorrectionCandidateFormat{

    private final List<CorrectionCandidate> inputCorrectionCandidates;
    private List<CorrectionCandidate> outputCorrectionCandidates;
    private List<CorrectionCandidate> outputAutomaticCorrections;


    public CorrectionCandidateTestFormat(List<CorrectionCandidate> inputCorrectionCandidates){
        this.inputCorrectionCandidates = Lists.newArrayList(inputCorrectionCandidates);
    }

    @Override
    public List<CorrectionCandidate> readCorrectionCandidates() throws Exception {
        return inputCorrectionCandidates;
    }


    @Override
    public List<CorrectionCandidate> readAutomaticCorrections() throws Exception {
        return null;
    }


    @Override
    public void writeCandidates(List<CorrectionCandidate> corrections, List<CorrectionCandidate> automaticCorrections) {
        outputCorrectionCandidates = Lists.newArrayList(corrections);
        outputAutomaticCorrections = Lists.newArrayList(automaticCorrections);
    }


    public List<CorrectionCandidate> getOutputCorrectionCandidates() {
        return outputCorrectionCandidates;
    }


    public List<CorrectionCandidate> getAutomaticCorrections() {
        return outputAutomaticCorrections;
    }
}
