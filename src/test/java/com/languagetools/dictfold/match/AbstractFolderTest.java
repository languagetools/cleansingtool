package com.languagetools.dictfold.match;


import com.languagetools.dictfold.AbstractIndexTest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public abstract class AbstractFolderTest extends AbstractIndexTest {


    public AbstractFolder folder;

    @Before
    public void setUpTestIndex() {
        createTestIndex();
        folder = createFolder();
        folder.initializeSearcher();
    }

    @Test
    public void indexIsReadCorrectly() {
        assertEquals(9, folder.getNumDocs());
    }

    @Test
    public void standardQueryReturnsCorrectResult() {
        assertEquals(33990121, folder.getTermFrequency("der"));
    }


    public abstract AbstractFolder createFolder();

}
