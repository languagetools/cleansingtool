package com.languagetools.dictfold.match.name;

import com.google.common.collect.Lists;
import com.languagetools.dictfold.CorrectionCandidate;
import com.languagetools.dictfold.TermFrequency;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class NameVariantMatchFoldingTaskTest {

    private NameVariantMatchFoldingTask foldingTask;
    private CorrectionCandidate correctionCandidate;

    @Test
    public void createEmptyCorrectionCandidateIfMatchexIsNull(){
        TermFrequency termFrequency = TermFrequency.of("Meier, Max J", 1);
        givenNameVariantFoldingTask(termFrequency);
        whenCorrectionCandidateIsCreated(null);
        thenTheCorrectionCandidateConsistsOf(termFrequency, null);
    }


    @Test
    public void createEmptyCorrectionCandidateIfMatchesIsEmpty(){
        TermFrequency termFrequency = TermFrequency.of("Meier, Max J", 1);
        givenNameVariantFoldingTask(termFrequency);
        whenCorrectionCandidateIsCreated(Collections.<TermFrequency>emptyList());
        thenTheCorrectionCandidateConsistsOf(termFrequency, null);
    }


    @Test
    public void targetTermFrequencyHasToBeGreaterThanTheSourceTermFrequency(){
        TermFrequency termFrequency = TermFrequency.of("Meier, Max J", 2);
        givenNameVariantFoldingTask(termFrequency);
        whenCorrectionCandidateIsCreated(Lists.newArrayList(termFrequency, TermFrequency.of("Meier, Max J", 1)));
        thenTheCorrectionCandidateConsistsOf(termFrequency, null);
    }


    @Test
    public void bestMatchHasHighestFrequency(){
        TermFrequency termFrequency = TermFrequency.of("Meier, Max J", 1);
        List<TermFrequency> matches =
                Lists.newArrayList(TermFrequency.of("Meier, M J", 2), TermFrequency.of("Meier, M Joe", 3),
                                   TermFrequency.of("Meier, M Jill", 4));

        givenNameVariantFoldingTask(termFrequency);
        whenCorrectionCandidateIsCreated(matches);
        thenTheCorrectionCandidateConsistsOf(termFrequency, TermFrequency.of("Meier, M Jill", 4));
    }

    @Test
    public void noCorrectionIfFirstThreeLettersOfFirstNamePartsDoNotMatch(){
        TermFrequency termFrequency = TermFrequency.of("Meier, Max Josef", 1);
        givenNameVariantFoldingTask(termFrequency);
        whenCorrectionCandidateIsCreated(Lists.newArrayList(TermFrequency.of("Meier, Mäx Janis", 2)));
        thenTheCorrectionCandidateConsistsOf(termFrequency, null);
    }


    @Test
    public void prefixMatchWorksOnAcronyms(){
        TermFrequency termFrequency = TermFrequency.of("Meier, Christian-J.", 1);
        givenNameVariantFoldingTask(termFrequency);
        whenCorrectionCandidateIsCreated(Lists.newArrayList(TermFrequency.of("Meier, ch. Josef", 2)));
        thenTheCorrectionCandidateConsistsOf(termFrequency, TermFrequency.of("Meier, ch. Josef", 2));
    }


    @Test
    public void prefixMatchDoesNotNormalizeUmlauts(){
        TermFrequency termFrequency = TermFrequency.of("Meier, Cös J", 1);
        givenNameVariantFoldingTask(termFrequency);
        whenCorrectionCandidateIsCreated(Lists.newArrayList(TermFrequency.of("Meier, choes. Josef", 2)));
        thenTheCorrectionCandidateConsistsOf(termFrequency, null);
    }


    @Test
    public void matchIfSingleTermInFirstNameOfOriginalTermIsAnAcronym(){
        TermFrequency termFrequency = TermFrequency.of("Meier, Ch.", 1);
        givenNameVariantFoldingTask(termFrequency);
        whenCorrectionCandidateIsCreated(Lists.newArrayList(TermFrequency.of("Meier, christian", 2)));
        thenTheCorrectionCandidateConsistsOf(termFrequency, TermFrequency.of("Meier, christian", 2));
    }


    @Test
    public void noMatchIfSingleTermInFirstNameOfTargetTermIsAnAcronym(){
        TermFrequency termFrequency = TermFrequency.of("Meier, Christian.", 1);
        givenNameVariantFoldingTask(termFrequency);
        whenCorrectionCandidateIsCreated(Lists.newArrayList(TermFrequency.of("Meier, C.", 2)));
        thenTheCorrectionCandidateConsistsOf(termFrequency, null);
    }


    @Test
    public void oneSingleTermFirstNamesAreNoAcronyms(){
        TermFrequency termFrequency = TermFrequency.of("Meier, Christian", 1);
        givenNameVariantFoldingTask(termFrequency);
        whenCorrectionCandidateIsCreated(Lists.newArrayList(TermFrequency.of("Meier, Christiane", 2)));
        thenTheCorrectionCandidateConsistsOf(termFrequency, null);
    }


    @Test
    public void firstFiveCandidatesSortedByTermFrequencyAreConsidered(){
        TermFrequency termFrequency = TermFrequency.of("Meier, Max Josef", 1);

        TermFrequency termFrequencyToIgnore = TermFrequency.of("Meier, Moi Johanna", 10);
        List<TermFrequency> matches = Lists.newArrayList(termFrequencyToIgnore, termFrequencyToIgnore,
                                                         termFrequencyToIgnore, termFrequencyToIgnore,
                                                         TermFrequency.of("Meier, M. Jossuf", 3),
                                                         TermFrequency.of("Meier, m.-J", 4));

        givenNameVariantFoldingTask(termFrequency);
        whenCorrectionCandidateIsCreated(matches);
        thenTheCorrectionCandidateConsistsOf(termFrequency, TermFrequency.of("Meier, m.-J", 4));
    }


    @Test
    public void sixthCandidateSortedByTermFrequencyNotConsidered(){
        TermFrequency termFrequency = TermFrequency.of("Meier, Max Josef", 1);
        TermFrequency termFrequencyToIgnore = TermFrequency.of("Meier, Moithilda Johanna", 10);
        List<TermFrequency> matches = Lists.newArrayList(termFrequencyToIgnore, termFrequencyToIgnore,
                termFrequencyToIgnore, termFrequencyToIgnore,termFrequencyToIgnore,
                TermFrequency.of("Meier, M.-J", 4));

        givenNameVariantFoldingTask(termFrequency);
        whenCorrectionCandidateIsCreated(matches);
        thenTheCorrectionCandidateConsistsOf(termFrequency, null);
    }


    private void thenTheCorrectionCandidateConsistsOf(TermFrequency termFrequency, TermFrequency targetTerm) {
        assertEquals(CorrectionCandidate.of(termFrequency, targetTerm, "FN"), correctionCandidate);
    }


    private void whenCorrectionCandidateIsCreated(List<TermFrequency> matches) {
        correctionCandidate = foldingTask.createCorrectionCandidate(matches);
    }

    private void givenNameVariantFoldingTask(TermFrequency foo) {
        foldingTask = new NameVariantMatchFoldingTask(new NameVariantMatchFolder(null), foo);
    }

}