package com.languagetools.dictfold.match.name;

import org.junit.Test;

import static org.junit.Assert.*;

public class NameMatchUtilsTest {


    @Test
    public void abbreviationIsNotChangedByFirstNameNormalization(){
        assertEquals("j", NameMatchUtils.abbreviateFirstName("j"));
    }


    @Test
    public void isLowerCasedByFirstNameNormalization(){
        assertEquals("j", NameMatchUtils.abbreviateFirstName("J"));
    }


    @Test
    public void umlautsAreLowerCasedByFirstNameNormalization(){
        assertEquals("ä", NameMatchUtils.abbreviateFirstName("ä"));
    }


    @Test
    public void whitespacesAreNormalizedByFirstNameNormalization(){
        assertEquals("j r", NameMatchUtils.abbreviateFirstName(" j  r "));
    }


    @Test
    public void whitespacesAreNormalizedForBlankStringByFirstNameNormalization(){
        assertTrue(NameMatchUtils.abbreviateFirstName("  ").isEmpty());
    }

    @Test
    public void namesAreAbbreviatedByFirstNameNormalization(){
        assertEquals("j r", NameMatchUtils.abbreviateFirstName("John Ralf"));
    }


    @Test
    public void semiAbbreviatednamesAreAbbreviatedByFirstNameNormalization(){
        assertEquals("j r", NameMatchUtils.abbreviateFirstName("John R"));
    }


    @Test
    public void abbreviationColonsAreErasedByFirstNameNormalization(){
        assertEquals("j r r", NameMatchUtils.abbreviateFirstName("J.  R.Ralf"));
    }


    @Test
    public void mulitpleColonsAreErasedByFirstNameNormalization(){
        assertEquals("j", NameMatchUtils.abbreviateFirstName("J.. "));
    }


    @Test
    public void separatingDashesAreErasedByFirstNameNormalization(){
        assertEquals("r m", NameMatchUtils.abbreviateFirstName("Rainer-Maria"));
    }


    @Test
    public void justOneFirstName(){
        assertEquals(1, NameMatchUtils.numberOfFirstNames(" Maik "));
    }


    @Test
    public void emptyFirstNamePartIsNotCounted(){
        assertEquals(3, NameMatchUtils.numberOfFirstNames("J.  R.R"));
    }


    @Test(expected = IllegalArgumentException.class)
    public void getLastNameThrowsForNull(){
        NameMatchUtils.getLastName(null);
    }


    @Test(expected = IllegalArgumentException.class)
    public void getLastNameForBlankInput(){
        NameMatchUtils.getLastName("  ");
    }

    @Test
    public void getLastNameForMissingFirstName(){
        assertEquals("meier", NameMatchUtils.getLastName("meier"));
    }


    @Test
    public void getLastNameFromFullName(){
        assertEquals("meier", NameMatchUtils.getLastName(" meier , franz"));
    }


    @Test
    public void getLastNameIsTrimmedToEmpty(){
        assertEquals("", NameMatchUtils.getLastName("   , franz"));
    }


    @Test(expected = IllegalArgumentException.class)
    public void getFirstNameForNull(){
        NameMatchUtils.getFirstName(null);
    }


    @Test(expected = IllegalArgumentException.class)
    public void getFirstNameForBlankInput(){
        NameMatchUtils.getFirstName("  ");
    }


    @Test(expected = IllegalArgumentException.class)
    public void getFirstNameIfNoFirstNameExists(){
        NameMatchUtils.getFirstName("meier");
    }


    @Test
    public void getFirstNameFromFullName(){
        assertEquals("Franz", NameMatchUtils.getFirstName("Meier, Franz"));
    }


    @Test
    public void getEmptyFirstNameIsTrimmedToEmpty(){
        assertEquals("", NameMatchUtils.getFirstName("Meier,   "));
    }


    @Test(expected = IllegalArgumentException.class)
    public void isFullNameCandidateThrowsOnNullInput(){
        NameMatchUtils.isFullNameCandidate(null);
    }


    @Test(expected = IllegalArgumentException.class)
    public void isFullNameCandidateThrowsOnBlankInput(){
        NameMatchUtils.isFullNameCandidate("  ");
    }


    @Test
    public void fullNameCandidateNeedsEnoughNameParts(){
        assertFalse(NameMatchUtils.isFullNameCandidate("meier"));
    }


    @Test
    public void fullNameCandidateMustNotHaveTooMuchNameParts(){
        assertFalse(NameMatchUtils.isFullNameCandidate("meier, Franz, Xaver"));
    }


    @Test
    public void fullNameCandidateMustExceedAMinimumOfFirstNameChars(){
        assertFalse(NameMatchUtils.isFullNameCandidate("Ji,"));
    }


    @Test
    public void fullNameCandidateMustExceedAMinimumOfNormalizedFirstNameChars(){
        assertFalse(NameMatchUtils.isFullNameCandidate("Jisss,  -_.(  :;!?/+& ´ "));
    }


    @Test
    public void fullNameCandidateMustExceedAMinimumOfLastNameCharacters(){
        assertFalse(NameMatchUtils.isFullNameCandidate("J,Franz"));
    }


    @Test
    public void fullNameCandidateMustExceedAMinimumOfNormalizedLastNameCharacters(){
        assertFalse(NameMatchUtils.isFullNameCandidate(" :;!?/+& ´ J.-_.(  ,Franz"));
    }


    @Test
    public void fullNameCandidateIsValid(){
        assertTrue(NameMatchUtils.isFullNameCandidate(" Ji, F. "));
    }

}