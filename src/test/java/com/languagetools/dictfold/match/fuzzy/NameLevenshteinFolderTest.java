package com.languagetools.dictfold.match.fuzzy;


import com.languagetools.dictfold.TermFrequency;
import com.languagetools.dictfold.configuration.LevenshteinConfiguration;
import com.languagetools.dictfold.match.AbstractFolder;
import com.languagetools.dictfold.match.AbstractFolderTest;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class NameLevenshteinFolderTest extends AbstractFolderTest {


        @Test
        public void matchForFuzzyLastName() throws IOException {
            List<TermFrequency> matches = folder.getMatches("blu, foo");
            assertEquals(1, matches.size());
        }


        @Test
        public void matchForFuzzyFirstName() throws IOException {
            List<TermFrequency> matches = folder.getMatches("bla, foi");
            assertEquals(1, matches.size());
        }


        @Test
        public void doNotMatchForFuzzyFirstAndLastName() throws IOException {
            List<TermFrequency> matches = folder.getMatches("blu, foi");
            assertTrue(matches.isEmpty());
        }


        @Test
        public void doNotMatchErrorsInFirstCharacterOfFirstName() throws IOException {
            List<TermFrequency> matches = folder.getMatches("bla, xoo");
            assertTrue(matches.isEmpty());
        }




        @Test
        public void ignoreNonFullNameCandidates() {
            assertFalse(folder.shallStartFoldingTaskFor(TermFrequency.of("foo", 2)));
        }


        @Test
        public void considerFullNameCandidates() {
            assertTrue(folder.shallStartFoldingTaskFor(TermFrequency.of("foo, bar", 2)));
        }


        @Override
        public AbstractFolder createFolder() {
            return new NameLevenshteinFolder(indexDirectory,
                    new LevenshteinConfiguration.Builder().minimalTermLength(1).frequencyThreshold(Integer.MAX_VALUE).build());
        }

}
