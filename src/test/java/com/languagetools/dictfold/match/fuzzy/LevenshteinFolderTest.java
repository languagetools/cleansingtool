package com.languagetools.dictfold.match.fuzzy;

import com.languagetools.dictfold.TermFrequency;
import com.languagetools.dictfold.configuration.LevenshteinConfiguration;
import com.languagetools.dictfold.match.AbstractFolder;
import com.languagetools.dictfold.match.AbstractFolderTest;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LevenshteinFolderTest extends AbstractFolderTest {

    @Test
    public void allApproximateMatchesAreCollected() throws IOException {
        List<TermFrequency> matches = folder.getMatches("in");
        assertEquals(2, matches.size());
        assertEquals("in", matches.get(0).getTerm());
        assertEquals(20884676, matches.get(0).getFrequency());
    }

    @Test
    public void noMatchForFullName() throws IOException {
        List<TermFrequency> matches = folder.getMatches("blu, foo");
        assertEquals(1, matches.size());
        assertFalse(folder.shallStartFoldingTaskFor(matches.get(0)));
    }


    @Test
    public void doNotMatchesWithMultipleEdits() throws IOException {
        List<TermFrequency> matches = folder.getMatches("blu, foi");
        assertTrue(matches.isEmpty());
    }


    @Override
    public AbstractFolder createFolder() {
            return new LevenshteinFolder(indexDirectory, new LevenshteinConfiguration.Builder().frequencyThreshold(Integer.MAX_VALUE).build());

    }
}
