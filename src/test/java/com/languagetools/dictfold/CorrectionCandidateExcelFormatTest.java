package com.languagetools.dictfold;

import com.google.common.collect.Lists;
import com.languagetools.dictfold.vlb.CorrectionCandidateExcelFormat;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class CorrectionCandidateExcelFormatTest {



    @Test
    public void correctionCandidatesAreRead() throws Exception {
        CorrectionCandidate firstCorrectionCandidate =
                new CorrectionCandidateTestBuilder().sourceTerm("Benedict, Ralph Paine").sourceFreq(1).targetTerm("Benedict, Robert P.").targetFreq(2).notApproved().build();
        CorrectionCandidate secondCorrectionCandidate =
                new CorrectionCandidateTestBuilder().sourceTerm("Bennett, Maxwell R").sourceFreq(0).targetTerm("Bennett, Matthew R.").targetFreq(0).notApproved().build();
        CorrectionCandidate thirdCorrectionCandidate =
                new CorrectionCandidateTestBuilder().sourceTerm("Ignoreme").sourceFreq(11).targetTerm("Ignoreme.").targetFreq(20).approved().comment("für Testzwecke").build();
        CorrectionCandidate forthCorrectionCandidate =
                new CorrectionCandidateTestBuilder().sourceTerm("Ignoleme").sourceFreq(1).targetTerm("Ignoreme.").targetFreq(59).comment("für Testzwecke").build();
        List<CorrectionCandidate> expectedCandidates = Lists.newArrayList(firstCorrectionCandidate, secondCorrectionCandidate,
                                                                          thirdCorrectionCandidate, forthCorrectionCandidate);
        assertEquals(expectedCandidates, new CorrectionCandidateExcelFormat(getExcelTestFile("validCorrectionCandidates.xlsx"), null).readCorrectionCandidates());
    }


    public static File getExcelTestFile(String name) throws URISyntaxException {
        return new File(CorrectionCandidateExcelFormat.class.getResource(name).toURI());
    }


    @Test(expected = IllegalArgumentException.class)
    public void fileWithMissingHeaderConfigurationIsIgnored() throws Exception {
        CorrectionCandidate firstCorrectionCandidate =
                new CorrectionCandidateTestBuilder().sourceTerm("foo").sourceFreq(1).targetTerm("foo.+").targetFreq(2).build();
        CorrectionCandidate secondCorrectionCandidate =
                new CorrectionCandidateTestBuilder().sourceTerm("foo").sourceFreq(1).targetTerm("foo.").targetFreq(3).build();
        List<CorrectionCandidate> expectedCandidates = Lists.newArrayList(firstCorrectionCandidate, secondCorrectionCandidate);
        assertEquals(expectedCandidates, new CorrectionCandidateExcelFormat(getExcelTestFile("missingHeader.xlsx"), null).readCorrectionCandidates());
    }


    @Test(expected = IllegalArgumentException.class)
    public void fileWithIncorrectHeaderConfigurationIsIgnored() throws Exception {
        CorrectionCandidate firstCorrectionCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").sourceFreq(1).targetTerm("foo.+").targetFreq(2).build();;
        CorrectionCandidate secondCorrectionCandidate = new CorrectionCandidateTestBuilder().sourceTerm("foo").sourceFreq(1).targetTerm("foo.").targetFreq(3).build();
        List<CorrectionCandidate> expectedCandidates = Lists.newArrayList(firstCorrectionCandidate, secondCorrectionCandidate);
        assertEquals(expectedCandidates, new CorrectionCandidateExcelFormat(getExcelTestFile("incorrectHeader.xlsx"), null).readCorrectionCandidates());
    }

}